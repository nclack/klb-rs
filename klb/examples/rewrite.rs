extern crate klb;
extern crate time;

use klb::read::VolumeSource;

fn main() {
    if let Some(filename) = std::env::args().nth(1) {
        
        
        let start = time::precise_time_ns();
        let rdr = klb::read::open(&filename).unwrap();
        let buf=rdr.read_to_vec().unwrap();
        let end = time::precise_time_ns();
        println!("Reading {:?} Time {} ms", rdr.header.xyzct, (end-start) as f64 / 1000000f64);


        let writer=klb::write::open("data/outimg.klb").unwrap();
        // TODO: improve handling of variant scalar type in api
        use klb::header::ScalarType;
        fn cast<T>(p:&[u8])->&[T] {
            use std::mem::size_of;
            use std::slice::from_raw_parts;
            unsafe{from_raw_parts(&p[0] as *const u8 as *const T,p.len()/size_of::<T>())}
        }
        let start = time::precise_time_ns();
        match rdr.header.data_type {
            ScalarType::UInt8   => {let buf:&[u8 ]=cast(&buf); writer.write(buf,rdr.header.xyzct)},
            ScalarType::UInt16  => {let buf:&[u16]=cast(&buf); writer.write(buf,rdr.header.xyzct)},
            ScalarType::UInt32  => {let buf:&[u32]=cast(&buf); writer.write(buf,rdr.header.xyzct)},
            ScalarType::UInt64  => {let buf:&[u64]=cast(&buf); writer.write(buf,rdr.header.xyzct)},
            ScalarType::Int8    => {let buf:&[i8 ]=cast(&buf); writer.write(buf,rdr.header.xyzct)},
            ScalarType::Int16   => {let buf:&[i16]=cast(&buf); writer.write(buf,rdr.header.xyzct)},
            ScalarType::Int32   => {let buf:&[i32]=cast(&buf); writer.write(buf,rdr.header.xyzct)},
            ScalarType::Int64   => {let buf:&[i64]=cast(&buf); writer.write(buf,rdr.header.xyzct)},
            ScalarType::Float32 => {let buf:&[f32]=cast(&buf); writer.write(buf,rdr.header.xyzct)},
            ScalarType::Float64 => {let buf:&[f64]=cast(&buf); writer.write(buf,rdr.header.xyzct)},
        }.unwrap();
        let end = time::precise_time_ns();
        
        println!("Writing {:?} Time {} ms", rdr.header.xyzct, (end-start) as f64 / 1000000f64);
    } else {
        println!("expected a filename as an argument.");
    }
}