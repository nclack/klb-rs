extern crate rand;
extern crate num_cpus;
extern crate crossbeam;
extern crate libc;
extern crate klb;
extern crate time;

use self::rand::Rng;
use klb::{write, read, io};
use klb::header::{HeaderV0, ScalarType};

#[test]
fn klb_load() {
  use read::VolumeSource;
  // let h=read::with_nthreads(1).open("../data/img.klb").unwrap();
  let h=read::open("../data/img.klb").unwrap();
  println!("{:?}",h);
  assert_eq!(h.nbytes(),101*151*29*2);
  let mut buf=vec![0;h.nbytes()];
  h.read(&mut buf).unwrap();  
  // {
  //   use std::fs::File;
  //   use std::io::Write;
  //   File::create("dump.u16").and_then(|mut f| f.write(&buf)).unwrap();
  // }
  
  // cast to uint16
  let v=unsafe{std::slice::from_raw_parts(
    &buf[0] as *const u8 as *const u16,
    buf.len()/2)};  
  assert_eq!(v[0],0);  
  assert_eq!(v[v.len()-1],497);
}

fn cast<T>(p:& [u8])->& [T] {
    use std::mem::size_of;
    use std::slice::from_raw_parts;
    unsafe{from_raw_parts(&p[0] as *const u8 as *const T,p.len()/size_of::<T>())}
}

#[test]
fn klb_write() {
  use read::VolumeSource;
  use klb::header::ScalarType;

  let nthreads: usize = num_cpus::get();

  let rdr = read::with_nthreads(nthreads).open("../data/img.klb").unwrap(); 
  let buf=rdr.read_to_vec().unwrap();
  println!("READ offsets: {:?}",rdr.offsets);

  let writer = write::WriterOptions::
    from(&rdr.header)
    .with_nthreads(nthreads)
    .open("../data/outimg.klb")
    .unwrap();
  let res=match rdr.header.data_type {
      ScalarType::UInt16  => {
        let buf:&[u16]=cast(&buf);
        writer.write(buf,rdr.header.xyzct)
      },
      _ => panic!("Unexpected scalar type.")
  };
  match res {
    Err(err) => {
      use std::io::Write;
      writeln!(&mut ::std::io::stderr(), "Error: {}", err).unwrap()
    }
    Ok(_) => {
      // println!("wrote {} bytes", sz);
      let rerdr = read::with_nthreads(nthreads).open("../data/outimg.klb").unwrap();
      let buf2=rerdr.read_to_vec().unwrap();
      println!("equal headers: {:?}", rdr.header == rerdr.header);
      assert_eq!(rdr.offsets, rerdr.offsets);
      assert_eq!(&buf[..], &buf2[..]);
    }
  }
}

#[test]
#[ignore]
fn klb_fuzz() {
  use io::Reader;
  use std::fs;
  use std::io::{Write, Seek, SeekFrom};
  use klb::read::VolumeSource;

  let filename="../data/fuzz.klb";

  let mut rng = rand::thread_rng();

  //copy img.klb and confirm that it is a regular file.
  {
    let rdr = read::open("../data/img.klb").unwrap();
    let buf = rdr.read_to_vec().unwrap();
    let writer=write::WriterOptions::
      from(&rdr.header)
      .open(filename)
      .unwrap();
    match rdr.header.data_type {
        ScalarType::UInt16  => {
          let buf:&[f64]=cast(&buf); 
          writer.write(buf,rdr.header.xyzct)
        },
        _ => panic!("Unexpected scalar type.")
    }.unwrap();
  }

  println!("writing to file");
  
  let sz=fs::metadata(filename).unwrap().len();
  let mut f = fs::OpenOptions::new()
    .write(true)
    .open(filename)
    .unwrap();
  let mut i = 447;
  while i < sz {
    if f.seek(SeekFrom::Current(i as i64)).is_ok() {
      let iwrite = rng.gen_range(0, 100);
      f.write(vec![rng.gen(); rng.gen_range(1, 100)].as_slice())
        .unwrap();
      i += iwrite;
    }

    i += rng.gen_range(0, 100);
  }
  f.flush().unwrap();
  println!("checking fuzzed file. size: {}", sz);
  let f = io::ReadableFile::open(filename).expect("Unable to Open \"../data/fuzz.klb\"");
  let hdr = HeaderV0::read(&f).expect("Corrupt Header, unable to open");

  println!("{:?}", hdr);
  println!("reading...");
  fn read(path:&str)->Result<(klb::header::HeaderV0,Vec<u8>),klb::error::Error> {
    let rdr=read::open(path)?;
    let buf=rdr.read_to_vec()?;
    Ok((rdr.header,buf))
  }
  match read(filename) {
    Err(err) => println!("Fuzz Read Error: {}", err),
    Ok((header,buf)) => {
      println!("writing...");
      let writer=write::WriterOptions::
          from(&header)
          .open(filename)
          .unwrap();
      match header.data_type {
          ScalarType::UInt16  => {let buf:&[f64]=cast(&buf); writer.write(buf,header.xyzct)},
          _ => panic!("Unexpected scalar type.")
      }.unwrap();
    }
  }
}

fn build_rand(nthreads: usize, vol: &[u8]) {
  let nt = if nthreads == -1i32 as usize {
    num_cpus::get()
  } else {
    nthreads
  };
  crossbeam::scope(|scope| for i in 0..nt {
    scope.spawn(move || {
      let mut rng = rand::thread_rng();
      let div = vol.len() / nt;
      let off = i * div;

      let len = if i == nt - 1 { vol.len() } else { off + div };

      let buf = vol as *const [u8] as *mut [u8];
      unsafe {
        rng.fill_bytes(&mut (*buf)[off..len]);
      }
    });
  });
}

#[test]
// #[ignore]
fn klb_rand() {
  use klb::read::VolumeSource;

  let nthreads=num_cpus::get();

  let n=512u32;
  let mut v=vec![0u8;(n*n*n) as usize];

  build_rand(nthreads, &mut v);

  println!("writing...");
  let cfgs=vec![
    write::options().bzip(),
    write::options().zlib(),
    write::options().zstd(),
    write::options().uncompressed(),
  ];
  for cfg in cfgs {
    // write 
    {
      let writer=cfg
        // .with_nthreads(nthreads)
        .open("../data/bench.klb").unwrap();
      let t0=time::precise_time_ns();
      writer.write(&v,[n,n,n,1,1]).unwrap();
      let t1=time::precise_time_ns();
      println!("Wrote {} GB/s",v.len() as f32/((t1-t0) as f32))
    }

    println!("reading...");
    let rdr = read::open("../data/bench.klb").unwrap();
    
    let t0=time::precise_time_ns();
    let buf = rdr.read_to_vec().unwrap();
    let t1=time::precise_time_ns();
    println!("Read {} GB/s",buf.len() as f32/((t1-t0) as f32));
    
    assert_eq!(&rdr.header.xyzct,&[n,n,n,1,1]);
    assert_eq!(v.len(),buf.len());
    for (i,(ref a,ref b)) in v.iter().zip(&buf).enumerate() {
      if a!=b {
        println!("Difference at index {}",i);
      }
      assert_eq!(a,b);
    }
  } // end loop over cfgs
}
