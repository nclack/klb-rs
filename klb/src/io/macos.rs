#![cfg(target_os ="macos")]
extern crate nix;
use super::Error;
use self::nix::sys::stat::{fstat, Mode};

pub struct ReadableFile(::std::os::unix::io::RawFd);

impl super::Reader for ReadableFile {
  fn open(filename: &str) -> Result<Self, Error> {
    use self::nix::fcntl::{open, O_RDONLY};
    let h = try!{open(filename,O_RDONLY,Mode::empty())};
    Ok(ReadableFile(h))
  }

  fn read_raw(&self, buf: &mut [u8], offset: u64) -> Result<(), Error> {
    use self::nix::sys::uio::pread;
    try!(pread(self.0, buf, offset as i64));
    Ok(())
  }

  fn nbytes(&self) -> u64 {
    let stat = fstat(self.0).unwrap();
    stat.st_size as u64
  }
}

impl ::std::convert::From<self::nix::Error> for Error {
  fn from(source: self::nix::Error) -> Self {
    use std::error::Error;
    self::Error::IOError(source.description().to_owned())
  }
}

pub struct WritableFile(::std::os::unix::io::RawFd);

impl super::Writer for WritableFile {
  fn create(filename: &str) -> Result<Self, self::Error>
  where
    Self: Sized,
  {
    use self::nix::fcntl::{open, O_WRONLY, O_CREAT};
    let h = try!(open(filename, O_WRONLY | O_CREAT, Mode::empty()));
    Ok(WritableFile(h))
  }

  fn write_raw(&self, buf: &[u8], offset: u64) -> Result<(), self::Error>
  where
    Self: Sized,
  {
    use self::nix::sys::uio::pwrite;
    try!(pwrite(self.0, buf, offset as i64));
    Ok(())
  }
}
