mod win;
mod linux;
mod macos;

use std::marker::Sized;
use std::mem::size_of;

use super::Error;

pub trait Reader {
  fn open(filename: &str) -> Result<Self, self::Error>
  where
    Self: Sized;
  fn read_raw(&self, buf: &mut [u8], offset: u64) -> Result<(), self::Error>;
  fn nbytes(&self) -> u64;

  ///  Reads bytes into buf.  Uses type of buf to infer how many bytes to read.
  ///  Unsafe because buf could be anything and it completely overwrites the memory
  ///  passed in.  This will probably crash if you do something like pass a Vec type
  ///  in, for example.
  ///
  ///  Maybe there's a better way to constrain the type of T so as to
  ///  guarantee it's ok to obliterate.  For example, maybe I could just force it to
  ///  be a slice.
  unsafe fn read<T: Sized>(&self, buf: &mut T, offset: u64) -> Result<(), self::Error> {
    unsafe fn to_raw<T: Sized>(p: &mut T) -> &mut [u8] {
      ::std::slice::from_raw_parts_mut((p as *mut T) as *mut u8, size_of::<T>())
    }
    self.read_raw(to_raw(buf), offset)
  }

  fn read_vec<T: Sized>(&self, nitems: usize, offset: u64) -> Result<Vec<T>, self::Error> {
    unsafe fn to_raw<T: Sized>(v: &mut Vec<T>) -> &mut [u8] {
      ::std::slice::from_raw_parts_mut(
        (&mut v[0] as *mut T) as *mut u8,
        v.capacity() * size_of::<T>(),
      )
    }

    let mut v = Vec::<T>::with_capacity(nitems);
    unsafe { v.set_len(nitems) };
    try!(self.read_raw(unsafe { to_raw(&mut v) }, offset));
    Ok(v)
  }
}

pub trait Writer {
  fn create(filename: &str) -> Result<Self, self::Error>
  where
    Self: Sized;
  fn write_raw(&self, buf: &[u8], offset: u64) -> Result<(), self::Error>;

  fn write<T: Sized>(&self, buf: &T, offset: u64) -> Result<(), self::Error> {
    unsafe fn to_raw<T: Sized>(p: &T) -> &[u8] {
      ::std::slice::from_raw_parts((p as *const T) as *const u8, size_of::<T>())
    }
    self.write_raw(unsafe{to_raw(buf)}, offset)
  }

  fn write_slice<T: Sized>(&self,buf:&[T],offset:u64)->Result<(),self::Error> {
    unsafe fn to_raw<T: Sized>(p: &[T]) -> &[u8] {
      ::std::slice::from_raw_parts(
        &p[0] as *const T as *const u8, 
        p.len()*size_of::<T>())
    }
    self.write_raw(unsafe{to_raw(buf)}, offset)
  }
}

#[cfg(target_os = "windows")]
pub type ReadableFile = win::ReadableFile;
#[cfg(target_os = "windows")]
pub type WritableFile = win::WritableFile;

#[cfg(target_os = "linux")]
pub type ReadableFile = linux::ReadableFile;
#[cfg(target_os = "linux")]
pub type WritableFile = linux::WritableFile;

#[cfg(target_os = "macos")]
pub type ReadableFile = macos::ReadableFile;
#[cfg(target_os = "macos")]
pub type WritableFile = macos::WritableFile;

#[test]
fn test_reader_nbytes() {
  let file: ReadableFile = Reader::open("../data/img.klb").unwrap();
  assert_eq!(file.nbytes(), 382141);
}

#[test]
fn test_simple_read() {
  use std;
  let mut buf: [u8; 16] = unsafe { std::mem::uninitialized() };
  let file: ReadableFile = Reader::open("../data/img.klb").unwrap();
  unsafe { file.read(&mut buf, 0x2b) }.unwrap();

  let expected: &[u8] = b"Testing metadata";
  assert_eq!(buf, expected);
}
