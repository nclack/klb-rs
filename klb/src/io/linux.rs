#![cfg(target_os ="linux")]
extern crate nix;
use super::Error;

use self::nix::sys::stat::{fstat, Mode};
use self::nix::fcntl::{open, O_RDONLY, O_WRONLY, O_LARGEFILE, O_CREAT};
use self::nix::sys::uio::{pwrite,pread};

#[derive(Debug)]
pub struct ReadableFile(::std::os::unix::io::RawFd);

impl super::Reader for ReadableFile {
  fn open(filename: &str) -> Result<Self, Error> {
    let h= open(filename,O_RDONLY|O_LARGEFILE,Mode::empty())?;
    Ok(ReadableFile(h))
  }

  fn read_raw(&self, buf: &mut [u8], offset: u64) -> Result<(), Error> {
    pread(self.0, buf, offset as i64)?;
    Ok(())
  }

  fn nbytes(&self) -> u64 {
    let stat = fstat(self.0).unwrap();
    stat.st_size as u64
  }
}

impl ::std::convert::From<self::nix::Error> for Error {
  fn from(source: self::nix::Error) -> Self {
    use std::error::Error;
    self::Error::IOError(source.description().to_owned())
  }
}

#[derive(Debug)]
pub struct WritableFile(::std::os::unix::io::RawFd);

impl super::Writer for WritableFile {
  fn create(filename: &str) -> Result<Self, self::Error>
  where
    Self: Sized,
  {
    let h = open(
      filename,
      O_WRONLY | O_LARGEFILE | O_CREAT,
      Mode::empty(),
    )?;
    Ok(WritableFile(h))
  }

  fn write_raw(&self, buf: &[u8], offset: u64) -> Result<(), self::Error>
  where
    Self: Sized,
  {
    pwrite(self.0, buf, offset as i64)?;
    Ok(())
  }
}
