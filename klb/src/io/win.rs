#![cfg(target_family="windows")]
extern crate kernel32;
extern crate winapi;

use self::winapi::HANDLE;
use self::winapi::minwinbase::OVERLAPPED;

#[allow(unused_imports)]
use self::winapi::minwindef::{DWORD, TRUE, FALSE};
use std::ptr::null_mut;
use super::Error;
use self::kernel32::{CloseHandle, GetLastError};

struct Overlapped(OVERLAPPED);

impl Overlapped {
  fn new() -> Self {
    use self::kernel32::CreateEventW;
    Overlapped(OVERLAPPED {
      Internal: 0,
      InternalHigh: 0,
      Offset: 0,
      OffsetHigh: 0,
      hEvent: unsafe { CreateEventW(null_mut(), FALSE, FALSE, null_mut()) },
    })
  }

  fn offset(&mut self, x: u64) {
    struct Split {
      low: DWORD,
      high: DWORD,
    };
    let s: Split = unsafe { ::std::mem::transmute(x) };
    self.0.Offset = s.low;
    self.0.OffsetHigh = s.high;
  }

  fn wait(&self) {
    use self::kernel32::WaitForSingleObject;
    use self::winapi::winbase::INFINITE;
    unsafe { WaitForSingleObject(self.0.hEvent, INFINITE) };
  }
}

impl ::std::convert::AsMut<OVERLAPPED> for Overlapped {
  fn as_mut(&mut self) -> &mut OVERLAPPED {
    &mut self.0
  }
}

impl Drop for Overlapped {
  fn drop(&mut self) {
    unsafe { CloseHandle(self.0.hEvent) };
  }
}

#[derive(Debug)]
pub struct ReadableFile {
  handle: HANDLE,
}

impl super::Reader for ReadableFile {
  fn open(filename: &str) -> Result<Self, Error> {
    use std::ffi::OsStr;
    use std::os::windows::ffi::OsStrExt;
    use std::iter::once;
    use self::winapi::shlobj::INVALID_HANDLE_VALUE;
    use self::winapi::fileapi::OPEN_EXISTING;
    use self::winapi::winbase::FILE_FLAG_OVERLAPPED;
    use self::winapi::winnt::{GENERIC_READ, FILE_SHARE_READ, FILE_SHARE_WRITE,
                              FILE_ATTRIBUTE_NORMAL};

    let wide: Vec<u16> = OsStr::new(filename).encode_wide().chain(once(0)).collect();
    let h = unsafe {
      kernel32::CreateFileW(
        wide.as_ptr(),
        GENERIC_READ,
        FILE_SHARE_READ | FILE_SHARE_WRITE,
        null_mut(),
        OPEN_EXISTING,
        FILE_ATTRIBUTE_NORMAL | FILE_FLAG_OVERLAPPED,
        null_mut(),
      )
    };
    if h == INVALID_HANDLE_VALUE {
      Err(Error::CouldNotOpenForRead(filename.to_owned()))
    } else {
      Ok(ReadableFile { handle: h })
    }
  }

  fn read_raw(&self, buf: &mut [u8], offset: u64) -> Result<(), self::Error> {
    use self::kernel32::ReadFile;
    use std::os::raw::c_void;
    let mut o = Overlapped::new();
    let nbytes = buf.len();
    let mut pos = offset;

    // ReadFile only reads up to 2GB at a time (largest DWORD).
    // So we read in chunks.
    //
    // The use of OVERLAPPED seems a little silly here, since
    // we immediately block, but it does a couple things for us:
    //
    // 1. We opened the file for async use so that this read function
    //    could be called from seperate threads.  This requires use of
    //    OVERLAPPED object.
    // 2. The OVERLAPPED object allows us to atomically read from a
    //    given offset similar to the linux pread() function.
    let n = nbytes as DWORD;
    for chunk in buf.chunks(n as usize) {
      let mut nread = unsafe { ::std::mem::uninitialized() };
      o.offset(pos);
      unsafe {
        ReadFile(
          self.handle,
          chunk.as_ptr() as *mut c_void,
          n,
          &mut nread,
          o.as_mut(),
        )
      }; // TODO: returns BOOL, handle errors
      o.wait();

      pos += chunk.len() as u64;
    }
    Ok(())
  }

  fn nbytes(&self) -> u64 {
    use self::winapi::winnt::LARGE_INTEGER;
    let mut sz: LARGE_INTEGER = unsafe { ::std::mem::uninitialized() };
    unsafe { kernel32::GetFileSizeEx(self.handle, &mut sz) }; // returns a bool to indicate success, but just ignore it
    sz as u64
  }
}

unsafe impl Send for ReadableFile {}
unsafe impl Sync for ReadableFile {}

impl Drop for ReadableFile {
  fn drop(&mut self) {
    unsafe { CloseHandle(self.handle) };
  }
}

//WRITABLEFILE----------------------
pub struct WritableFile {
  handle: HANDLE,
}

impl super::Writer for WritableFile {
  fn create(filename: &str) -> Result<Self, self::Error>
  where
    Self: Sized,
  {
    use std::ffi::OsStr;
    use std::iter::once;
    use std::os::windows::ffi::OsStrExt;
    use self::winapi::fileapi::CREATE_ALWAYS;
    use self::winapi::shlobj::INVALID_HANDLE_VALUE;
    use self::winapi::winbase::FILE_FLAG_OVERLAPPED;
    use self::winapi::winnt::{GENERIC_WRITE, FILE_SHARE_READ, FILE_SHARE_WRITE,
                              FILE_ATTRIBUTE_NORMAL};

    let wide_file: Vec<u16> = OsStr::new(filename).encode_wide().chain(once(0)).collect();
    let h = unsafe {
      kernel32::CreateFileW(
        wide_file.as_ptr(),
        GENERIC_WRITE,
        FILE_SHARE_READ | FILE_SHARE_WRITE,
        null_mut(),
        CREATE_ALWAYS,
        FILE_ATTRIBUTE_NORMAL | FILE_FLAG_OVERLAPPED,
        null_mut(),
      )
    };

    if h == INVALID_HANDLE_VALUE {
      Err(Error::CouldNotOpenForWrite(filename.to_owned()))
    } else {
      Ok(WritableFile { handle: h })
    }
  }

  /* TODO: check if WriteFile does the same thing */
  // ReadFile only reads up to 2GB at a time (largest DWORD).
  // So we read in chunks.
  //
  // The use of OVERLAPPED seems a little silly here, since
  // we immediately block, but it does a couple things for us:
  //
  // 1. We opened the file for async use so that this read function
  //    could be called from seperate threads.  This requires use of
  //    OVERLAPPED object.
  // 2. The OVERLAPPED object allows us to atomically read from a
  //    given offset similar to the linux pread() function.
  fn write_raw(&self, buf: &[u8], offset: u64) -> Result<(), self::Error> {
    use self::kernel32::WriteFile;
    use std::os::raw::c_void;

    let mut overlapped = Overlapped::new();
    let mut pos = offset;
    let n = buf.len() as DWORD;

    for chunk in buf.chunks(n as usize) {
      let mut nwrit = unsafe { ::std::mem::uninitialized() };

      overlapped.offset(pos);
      unsafe {
        let res: i32 = WriteFile(
          self.handle,
          chunk.as_ptr() as *mut c_void,
          n,
          &mut nwrit,
          overlapped.as_mut(),
        );
        if res == 0 && GetLastError() != 997 {
          return Err(Error::IOError(
            format!("write_raw failed: Error Code {}", GetLastError()),
          ));
        }
      };
      overlapped.wait();

      pos += chunk.len() as u64;
    }
    Ok(())
  }
}

impl Drop for WritableFile {
  fn drop(&mut self) {
    unsafe { CloseHandle(self.handle) };
  }
}

unsafe impl Send for WritableFile {}
unsafe impl Sync for WritableFile {}
