extern crate num_cpus;
extern crate crossbeam;
extern crate spmc;

use std::default::Default;
use std::fmt::Debug;
use super::{Error,io};
use super::codec;

pub struct WriterOptions {
  nthreads: usize,
  encoding: codec::Identifier,
  block: [u32;5],
  description: String,
  pixel_size: [f32;5]
}

impl Default for WriterOptions {
  fn default()->Self {
    WriterOptions{
      nthreads: num_cpus::get()*2,
      encoding: codec::Identifier::ZStd,      
      block:[64,64,8,1,1],
      description: String::new(),
      pixel_size: [1.0, 1.0, 1.0, 1.0, 1.0],
    }
  }
}

use super::header::{HeaderV0,ScalarType,Version};

impl WriterOptions {
  pub fn open(self,path: &str) -> Result<Writer,Error> {
    let mut w=open(path)?;
    w.hints=self;
    Ok(w)
  } 
  pub fn with_nthreads(mut self,nthreads:usize)->Self {
    self.nthreads=nthreads;
    self
  }
  pub fn bzip(mut self)->Self {
    self.encoding=codec::Identifier::BZip2;
    self
  }
  pub fn zlib(mut self)->Self {
    self.encoding=codec::Identifier::ZLib;
    self
  }
  pub fn zstd(mut self)->Self {
    self.encoding=codec::Identifier::ZStd;
    self
  }
  pub fn uncompressed(mut self)->Self {
    self.encoding=codec::Identifier::None;
    self
  }
  pub fn with_block_shape(mut self,shape: [u32;5])->Self {
    self.block=shape;
    self
  }
  pub fn with_description(mut self, description: &str)->Self {
    self.description=description.to_owned();
    self
  }
  pub fn with_pixel_scale(mut self, scale: [f32;5])->Self {
    self.pixel_size=scale;
    self
  }
  pub fn into_header(&self,scalar_type:ScalarType,shape:[u32;5]) -> HeaderV0 {
    let d:&[u8]=truncate_bytes(&self.description,256);
    let mut desc:[u8;256]=[0;256];
    desc[..d.len()].copy_from_slice(d);
    HeaderV0{
      version: Version::V0, // TODO: why is this here?
      xyzct: shape,
      pixel_size: self.pixel_size,
      data_type: scalar_type,
      compression_type: self.encoding,
      description: desc,
      block_shape: self.block
    }
  }
}

impl<'a> From<&'a HeaderV0> for WriterOptions {
  fn from(h:&'a HeaderV0)->Self {
    let d=unsafe{String::from_utf8_unchecked(h.description.to_vec())};
    WriterOptions {
      encoding: h.compression_type,
      description: d,
      block: h.block_shape,
      pixel_size: h.pixel_size,
      ..Default::default()
    }
  }
}

fn truncate_bytes(s:&str,n:usize)->&[u8] {
  let mut out=s.as_bytes();
  if out.len()>256 {
    let mut found=false;
    'search: for i in 0..n {
      if s.is_char_boundary(n-i) {
        let(a,_)=s.split_at(i);
        out=a.as_bytes();
        found = true;
        break 'search;
      }      
    }
    if !found {
      // description string has no valid utf-8 break...
      // probably invalid data, but try to write it anyway
      out=&s.as_bytes()[..256];
    }
  }
  out
}

pub struct Writer {
  file: io::WritableFile,
  hints: WriterOptions
}

pub fn options()->WriterOptions{
  WriterOptions{..Default::default()}
}

pub fn open(filename: &str) -> Result<Writer,Error> {  
  Ok(Writer {
    file: io::Writer::create(filename)?,
    hints: options()
  })
}

pub trait HasTypeId {
  fn type_id()->ScalarType;
}

impl HasTypeId for u8  { fn type_id()->ScalarType {ScalarType::UInt8}}
impl HasTypeId for u16 { fn type_id()->ScalarType {ScalarType::UInt16}}
impl HasTypeId for u32 { fn type_id()->ScalarType {ScalarType::UInt32}}
impl HasTypeId for u64 { fn type_id()->ScalarType {ScalarType::UInt64}}
impl HasTypeId for i8  { fn type_id()->ScalarType {ScalarType::Int8}}
impl HasTypeId for i16 { fn type_id()->ScalarType {ScalarType::Int16}}
impl HasTypeId for i32 { fn type_id()->ScalarType {ScalarType::Int32}}
impl HasTypeId for i64 { fn type_id()->ScalarType {ScalarType::Int64}}
impl HasTypeId for f32 { fn type_id()->ScalarType {ScalarType::Float32}}
impl HasTypeId for f64 { fn type_id()->ScalarType {ScalarType::Float64}}

use std::slice::{from_raw_parts,from_raw_parts_mut};
use std::mem::size_of;

fn as_u8_slice<T:Sized>(v:&[T])->&[u8] {
  unsafe{from_raw_parts(&v[0] as *const T as *const u8,v.len()*size_of::<T>())}
}

fn as_u8_slice_mut<T:Sized>(v:&mut [T])->&mut [u8] {
  unsafe{from_raw_parts_mut(&mut v[0] as *mut T as *mut u8,v.len()*size_of::<T>())}
}

unsafe fn make_uninitialized_vec<T:Sized>(n:usize)->Vec<T> {
  let mut v=Vec::with_capacity(n);
  v.set_len(n);
  v
}

use std::sync::{Arc,Mutex,Condvar};

#[derive(Clone)]
struct Event(Arc<(Mutex<(bool,bool)>,Condvar)>);

impl Event {
  fn new()->Self {
    // hold bools for predicate and err condition
    Event(Arc::new((Mutex::new((false,false)),Condvar::new())))
  }
  fn notify(&self) {
    let &(ref lock, ref cvar) = &*self.0;
    let mut state = lock.lock().unwrap();
    *state = (true,state.1);
    cvar.notify_one();
  }
  fn abort(&self) {
    let &(ref lock, ref cvar) = &*self.0;
    let mut state = lock.lock().unwrap();
    *state = (state.0,true);
    cvar.notify_one();
  }
  fn wait(&self)->Option<()> {
    let &(ref lock, ref cvar) = &*self.0;
    let mut state = lock.lock().unwrap();
    while !state.0 && !state.1 {
        state = cvar.wait(state).unwrap();
    }
    if state.1 { // err condition
      None
    } else {
      *state=(false,false); // reset
      Some(())
    }
  }
}

// rust is such a pleasure
struct EverythingIsFine<T>{ptr:*mut T,len:usize}
unsafe impl<T> Sync for EverythingIsFine<T>{}
impl<T> EverythingIsFine<T> {
  fn new(buf:&mut [T])->Self {
    EverythingIsFine{ptr:&mut buf[0],len:buf.len()}
  }
  fn as_slice_mut(&self)->&mut [T] {
    use std::slice::from_raw_parts_mut;
    unsafe{from_raw_parts_mut(self.ptr,self.len)}
  }
}

/// Sort of a proof-of-principle use of spmc
#[test]
fn spmc_check() {
  use std::sync::{Arc,Mutex};
  let (tx,rx)=spmc::channel();
  let n=Arc::new(Mutex::new(0));
  crossbeam::scope(|scope| {
    for _ in 0..10 {
      let rx=rx.clone();
      let n=n.clone();
      scope.spawn(move || {
        let mut count=0;
        while let Ok(_) = rx.recv() {            
            count+=1;
        }
        let mut n=n.lock().unwrap();
        *n+=count;
      });
    }
    for i in 0..100 {
      tx.send(i).unwrap();
    }
    drop(tx); // important to drop here so receivers will close
  });
  assert_eq!(100,*n.lock().unwrap());
}

impl Writer {
  pub fn write<T:Debug>(self,data: &[T],shape:[u32;5])->Result<(),Error> 
      where T: HasTypeId+Sync
  {
    use super::read::{Region,Strides,StridedRef,StridedRefMut,BlockLattice};
    use super::io::Writer;
    use std::sync::mpsc;
    use std::mem::swap;
    use std::sync::atomic::{AtomicUsize,Ordering};
    use std::sync::{Barrier,Arc};
    use self::spmc::TryRecvError;
     
    let full_region=Region::from(&shape);
    let header=self.hints.into_header(T::type_id(),shape);
    let block_lattice=BlockLattice::from(&header);
    let block_strides_template=Strides::from(Region::from(&header.block_shape));
    let nblocks=block_lattice.nelem();
    let blocks_section_offset:u64=(HeaderV0::raw_size()+(nblocks*size_of::<u64>())) as u64;
    let nthreads=self.hints.nthreads;
    let barrier=Arc::new(Barrier::new(nthreads+1));
    let codec=header.compression_type;
    
    // TODO: take all these context vecs and break them into pointers/slices so
    //       rust doesn't do any locking on them and also to get around ownership
    //       rules
    let mut encoded_blocks_:Vec<Option<Vec<u8>>> = vec![None;nblocks];
    let encoded_blocks_=&EverythingIsFine::new(&mut encoded_blocks_);
    let update_event=Event::new();
    
    let (free_tx,free_rx)=spmc::channel();
    let (error_tx,error_rx)=mpsc::channel();

    // Generate buffers for holding encoded data
    // Do this before starting threads below so we can move free_tx into 
    // the writer thread.
    //
    // NOTE: If the compression threads get too far ahead of the writer
    // they'll drain this queue.  If a compressor gets an empty queue, it'll
    // allocate a new buffer.
    //
    // These buffers get sent to the writer via "encoded_blocks."  The writer
    // then returns them to the "free" channel when it's done.  So they get
    // recycled.
    for _ in 0..nthreads*2 {
      let encoded=Vec::with_capacity(block_strides_template.nelem()*size_of::<T>());
      free_tx.send(encoded).unwrap();
    }

    let iblock__=AtomicUsize::new(0); // block counter for compressor threads
    
    crossbeam::scope(|scope| {
      // WRITER THREAD
      {
        let update_event=update_event.clone();
        let error_tx=error_tx.clone();
        let encoded_blocks=encoded_blocks_.as_slice_mut();
        let barrier=barrier.clone();
        scope.spawn(move || {
          let mut current_block_offset=blocks_section_offset;
          let mut offsets_table:Vec<u64>=vec![0;nblocks];
          let offset_table_start=(nblocks*8+size_of::<HeaderV0>()-1) as u64;
          if let Err(e)=self.write_header(&header) {
            error_tx.send(e).unwrap();
            return;
          }
          barrier.wait();
          for iblock in 0..nblocks {
            // non-polling wait for update notification
            // on update see if the block we're waiting on has been filled
            // if not, wait for another update.  Otherwise, move on.            
            'wait_for_block: loop {
              if let Some(_)=encoded_blocks[iblock] {                 
                break 'wait_for_block;
              } else {
                // println!("WRITER: block {} - \tWait",iblock);
                if update_event.wait().is_none() {
                  // println!("WRITER: block {} - \tAbort wait",iblock);
                  return;
                }
              }
            }
            // println!("WRITER: block {} - writing",iblock);
            let mut encoded=None;
            swap(&mut encoded,&mut encoded_blocks[iblock]);        
            // If the unwrap() panics in the line below, something is really
            // wrong.
            let mut encoded=encoded.unwrap();
            if let Err(e)=self.file.write_raw(&encoded,current_block_offset) {
              error_tx.send(e).unwrap();
              return;
            }
            current_block_offset+=encoded.len() as u64;
            offsets_table[iblock]=current_block_offset-offset_table_start;
            // recycle the buffer
            unsafe{encoded.set_len(0)};
            // println!("WRITER: block {} - Recycling buffer.",iblock);
            free_tx.send(encoded).unwrap();        
          }
          drop(free_tx);
          // done writing blocks
          // write offets table
          if let Err(e)=self.file.write_slice(&offsets_table,HeaderV0::raw_size() as u64) {
            error_tx.send(e).unwrap();
            return;
          }
        });
      }
    
      // COMPRESSOR THREADS
    
      let iblock_=&iblock__;      
      for _ in 0..nthreads {
        let error_tx=error_tx.clone();
        let free_rx=free_rx.clone();        
        let update_event=update_event.clone();
        let barrier=barrier.clone();
        scope.spawn(move|| {
          // per thread resources
          let mut decoded:Vec<T>=Vec::with_capacity(block_strides_template.nelem());
          let mut packet:Vec<T>=unsafe{make_uninitialized_vec(2*block_strides_template.nelem())};
          // let mut encoded=Vec::with_capacity(block_strides_template.nelem()*size_of::<T>());
          let mut encoder=codec.to_encoder();
          barrier.wait();
          'block_processing: loop {
            let iblock=iblock_.fetch_add(1,Ordering::AcqRel);
            if iblock>=nblocks {
              break 'block_processing;
            }
            // println!("COMPRESSOR: block {} - waiting on buffer",iblock);
            let mut encoded=match free_rx.try_recv() {
              Ok(v) => v,
              Err(TryRecvError::Disconnected) => return,
              Err(TryRecvError::Empty) => {
                // println!("COMPRESSOR: block {} - Allocating",iblock);
                Vec::with_capacity(block_strides_template.nelem()*size_of::<T>())
              },              
            };
            // println!("COMPRESSOR: block {} - \tGot buffer (len {})",iblock,encoded.len());
            // copy the block to it's own buffer
            {
              let block=block_strides_template
                          .translate(&block_lattice.to_coord_px(iblock));
              unsafe{decoded.set_len(block.nelem())};
              let mut vdst=StridedRefMut{
                  buf: unsafe{from_raw_parts_mut(&mut decoded[0] as *mut T,decoded.len())},
                  desc:block
              }.clip(&full_region);
              let vsrc=StridedRef{
                buf: data,
                desc: Strides::from(full_region)
              };
              StridedRef::copy(&mut vdst,&vsrc);
              unsafe{decoded.set_len(vdst.desc.nelem())}; // properly resize for clipped block
            }
            // encode
            let mut stream=encoder.bind(as_u8_slice(&decoded));
            'encode: loop {
              match stream.encode(as_u8_slice_mut(&mut packet)) {
                Ok(Some(p)) => encoded.extend_from_slice(p),
                Ok(None) => break 'encode,
                Err(_) => {                  
                  // println!("COMPRESSOR: encoding error for block {}",iblock);
                  error_tx.send(Error::CompressFailure).unwrap();
                  update_event.abort();
                  break 'block_processing;
                }
              }
            }
            
            // post the encoded block
            let mut t=Some(encoded);            
            let encoded_blocks=encoded_blocks_.as_slice_mut();
            swap(&mut encoded_blocks[iblock],&mut t);
            // println!("COMPRESSOR: block {} - posting",iblock);
            update_event.notify();
          } // end block processing 
        }); // end compressor thread
      }
    });
    drop(error_tx);
    // Collect any errors and wait for things to finish
    if let Some(err)=error_rx.recv().iter().next() {
      Err(err.clone())
    } else {
      Ok(())
    }
  }

  fn write_header(&self,header:&HeaderV0)->Result<(),Error> {
    use super::header::RawHeader;
    use super::io::Writer;
    let h=RawHeader::from(header);
    self.file.write(&h, 0u64)?;
    Ok(())
  }
}