#![allow(dead_code)]
extern crate libc;

use super::Error;
use std::fmt::Debug;
use std::mem;

pub const KLB_DATA_DIMS: usize = 5;
pub const KLB_METADATA_SIZE: usize = 256;

//VERSION-------------------------------------
#[derive(Clone, Copy)]
pub enum Version {
  V0,
}

impl Version {
  pub fn try_from_raw(v: u8) -> Result<Self, Error> {
    match v {
      2 => Ok(Version::V0),
      _ => Err(Error::BadVersionId(v)),
    }
  }
}

impl From<Version> for u8 {
  fn from(v: Version) -> u8 {
    match v {
      Version::V0 => 2,
    }
  }
}

//SCALARTYPE---------------------------------
#[derive(Debug, Clone, Copy, PartialEq, Eq)]
pub enum ScalarType {
  UInt8,
  UInt16,
  UInt32,
  UInt64,
  Int8,
  Int16,
  Int32,
  Int64,
  Float32,
  Float64,
}

impl ScalarType {
  pub fn try_from_raw(val: u8) -> Result<ScalarType, Error> {
    match val {
      0 => Ok(ScalarType::UInt8),
      1 => Ok(ScalarType::UInt16),
      2 => Ok(ScalarType::UInt32),
      3 => Ok(ScalarType::UInt64),
      4 => Ok(ScalarType::Int8),
      5 => Ok(ScalarType::Int16),
      6 => Ok(ScalarType::Int32),
      7 => Ok(ScalarType::Int64),
      8 => Ok(ScalarType::Float32),
      9 => Ok(ScalarType::Float64),
      _ => Err(Error::BadScalarTypeId(val)),
    }
  }

  pub fn size(&self) -> usize {
    match *self {
      ScalarType::UInt8 |
      ScalarType::Int8 => 1,
      ScalarType::UInt16 |
      ScalarType::Int16 => 2,
      ScalarType::UInt32 |
      ScalarType::Int32 |
      ScalarType::Float32 => 4,
      ScalarType::UInt64 |
      ScalarType::Int64 |
      ScalarType::Float64 => 8,
    }
  }
}

impl From<ScalarType> for u8 {
  fn from(t: ScalarType) -> u8 {
    match t {
      ScalarType::UInt8 => 0,
      ScalarType::UInt16 => 1,
      ScalarType::UInt32 => 2,
      ScalarType::UInt64 => 3,
      ScalarType::Int8 => 4,
      ScalarType::Int16 => 5,
      ScalarType::Int32 => 6,
      ScalarType::Int64 => 7,
      ScalarType::Float32 => 8,
      ScalarType::Float64 => 9,
    }
  }
}

//HEADERV0----------------------------------
/// The binary format for the file header
#[repr(C, packed)]
pub struct RawHeader {
  pub ver: u8,
  pub xyzct: [u32; KLB_DATA_DIMS],
  pub px_sz: [f32; KLB_DATA_DIMS],
  pub dt: u8,
  pub ct: u8,
  pub desc: [u8; KLB_METADATA_SIZE],
  pub bs: [u32; KLB_DATA_DIMS],
}

impl RawHeader {
  pub fn from(h: &HeaderV0) -> RawHeader {
    RawHeader {
      ver: h.version.into(),
      xyzct: h.xyzct,
      px_sz: h.pixel_size,
      dt: h.data_type.into(),
      ct: h.compression_type.into(),
      desc: h.description,
      bs: h.block_shape,
    }
  }
}

/// Representation of the file header
pub struct HeaderV0 {
  pub version: Version, // TODO: why is this here?
  pub xyzct: [u32; KLB_DATA_DIMS],
  pub pixel_size: [f32; KLB_DATA_DIMS],
  pub data_type: ScalarType,
  pub compression_type: super::codec::Identifier,
  pub description: [u8; KLB_METADATA_SIZE],
  pub block_shape: [u32; KLB_DATA_DIMS],
}

impl From<super::codec::BadCompressionIdentifier> for Error {
  fn from(e: super::codec::BadCompressionIdentifier)->Self {
    Error::BadCompressionTypeId(e.id())
  }
}

impl HeaderV0 {
  pub fn try_from_raw(rh: RawHeader) -> Result<Self, Error> {
    Ok(HeaderV0 {
      version: Version::try_from_raw(rh.ver)?,
      xyzct: rh.xyzct,
      pixel_size: rh.px_sz,
      data_type: ScalarType::try_from_raw(rh.dt)?,
      compression_type: super::codec::Identifier::try_from_raw(rh.ct)?,
      description: rh.desc,
      block_shape: rh.bs,
    })
  }

  pub fn read<R: super::io::Reader>(file: &R) -> Result<Self, Error> {
    let mut raw_hdr: RawHeader = unsafe { mem::uninitialized() };
    unsafe { file.read(&mut raw_hdr, 0) }?;
    Ok(Self::try_from_raw(raw_hdr)?)
  }

  pub fn nblocks(&self) -> usize {
    self.xyzct.iter().zip(self.block_shape.iter()).fold(1, |n,
     (&e, &b)| {
      n * ((e + b - 1) / b)
    }) as usize
  }

  pub fn block_nelem(&self) -> usize {
    self.block_shape.into_iter().product::<u32>() as usize
  }

  pub fn block_nbytes(&self) -> usize {
    self.block_nelem()*self.data_type.size()
  }

  //TODO: remove this function
  //WARNING, vol.data is uninitialized.  Try not to read
  pub fn alloc_vol(&self) -> Result<(*mut u8, usize), Error> {
    let len = self.strides()[5];

    let ptr = unsafe { libc::malloc(len) };
    if ptr.is_null() {
      Err(Error::MemError("Unable to malloc volume".to_owned()))
    } else {
      Ok((ptr as *mut u8, len))
    }
  }

  pub fn strides(&self) -> [usize; KLB_DATA_DIMS + 1] {
    let mut strides: [usize; KLB_DATA_DIMS + 1] = Default::default();

    strides[0] = self.data_type.size();

    for (i, &dim) in self.xyzct.iter().enumerate().take(KLB_DATA_DIMS) {
      strides[i + 1] = dim as usize * strides[i];
    }
    strides
  }

  pub fn block_strides(&self, block_shape: &[usize; KLB_DATA_DIMS]) -> [usize; 6] {
    let mut strides: [usize; 6] = Default::default();
    strides[0] = self.data_type.size();
    for i in 0..5 {
      strides[i + 1] = block_shape[i] as usize * strides[i];
    }
    strides
  }

  //returns block offset and upper bound as usize arrays in pixel values
  pub fn unwrap_iblock(&self, ib: usize) -> ([usize; 5], [usize; 5]) {
    use std::cmp::min;

    let divstripe;
    {
      let mut divs = [0usize; KLB_DATA_DIMS];
      //calculate divs
      for (i, (&dim, &shape)) in
        self
          .xyzct
          .iter()
          .zip(self.block_shape.iter())
          .enumerate()
          .take(KLB_DATA_DIMS)
      {
        divs[i] = (dim / shape) as usize;
        if dim % shape > 0 {
          divs[i] += 1;
        }
      }
      //build stripes from divs
      let mut ds = [0usize; KLB_DATA_DIMS + 1];
      ds[0] = 1;
      for i in 0..KLB_DATA_DIMS {
        ds[i + 1] = ds[i] * divs[i];
      }
      divstripe = ds
    }

    let mut offsets = [0usize; KLB_DATA_DIMS];
    let mut ub = [0usize; KLB_DATA_DIMS];
    let mut iblock = ib;

    for i in (0..KLB_DATA_DIMS).rev() {
      let d = divstripe[i];
      if iblock >= d {
        offsets[i] = (iblock / d) * self.block_shape[i] as usize;
        iblock %= d;
      }
      ub[i] = min(
        offsets[i] + self.block_shape[i] as usize,
        self.xyzct[i] as usize,
      );
    }
    (offsets, ub)
  }

  pub fn raw_size() -> usize {
    ::std::mem::size_of::<RawHeader>()
  }

  fn nbytes_block(&self) -> usize {
    self.xyzct.iter().fold(1, |n, d| n * (*d as usize)) * self.data_type.size()
  }
}

impl PartialEq for HeaderV0 {
  fn eq(&self, other: &HeaderV0) -> bool {
    (self.xyzct == other.xyzct && self.pixel_size == other.pixel_size &&
       self.data_type == other.data_type && self.compression_type == other.compression_type &&
       self.block_shape == other.block_shape && self.description[..] == other.description[..])
  }
}

impl Clone for HeaderV0 {
  fn clone(&self) -> HeaderV0 {
    let mut desc = [0u8; KLB_METADATA_SIZE];
    (&mut desc[..]).copy_from_slice(&self.description[..]);
    HeaderV0 {
      version: self.version,
      xyzct: self.xyzct,
      pixel_size: self.pixel_size,
      data_type: self.data_type,
      compression_type: self.compression_type,
      description: desc,
      block_shape: self.block_shape,
    }
  }
}


impl Debug for HeaderV0 {
  fn fmt(&self, f: &mut ::std::fmt::Formatter) -> Result<(), ::std::fmt::Error> {
    write!(
      f,
      "HeaderV0 {{ version: {}, xyzct: {:?}, pixel_size: {:?}, data_type: {:?}, compression_type: {}, block_shape: {:?} }}",
      u8::from(self.version),
      self.xyzct,
      self.pixel_size,
      self.data_type,
      self.compression_type,
      self.block_shape
    )
  }
}

#[cfg(test)]
mod header_test {
  extern crate num_cpus;

  #[test]
  fn klb_read_header_1() {
    use super::super::codec::Identifier;
    use super::super::read;
    use super::{HeaderV0, ScalarType, Version};

    let mut expected = HeaderV0 {
      version: Version::V0,
      xyzct: [101, 151, 29, 1, 1],
      pixel_size: [1.0, 1.0, 1.0, 1.0, 1.0],
      data_type: ScalarType::UInt16,
      compression_type: Identifier::BZip2,
      description: [0; 256], //don't check this equals
      block_shape: [96, 96, 8, 1, 1],
    };
    {
      let mut d = &mut expected.description;
      let msg = b"Testing metadata";
      d[..msg.len()].clone_from_slice(msg);
    }
    let reader = read::open("../data/img.klb").unwrap();
    assert_eq!(reader.header.xyzct, expected.xyzct);
    assert_eq!(reader.header.pixel_size, expected.pixel_size);
    assert_eq!(reader.header.data_type, expected.data_type);
    assert_eq!(reader.header.compression_type, expected.compression_type);
    assert_eq!(reader.header.block_shape, expected.block_shape);
    assert_eq!(HeaderV0::raw_size(), 319); //3*4*5+256+2+1
    assert_eq!(reader.header.nblocks(), 16); //2*2*4
    let n = reader.header.nblocks() as u64;
    // first block should start at n * 8 + 319 = 128+319 = 447
    assert_eq!(reader.offsets[(n - 1) as usize] + n * 8 + 319, 382141); // last offset + header+nblocks*8+version_id should be the file size
  }
}
