#![allow(dead_code)]
extern crate crossbeam;
extern crate num_cpus;

use header::ScalarType;
use super::{HeaderV0, Error};

use std::mem::uninitialized;

#[derive(Clone,Copy,Default,Debug,PartialEq)]
pub struct Region {
  pub min:[i64;5], // inclusive 
  pub max:[i64;5]  // exclusive
}

impl Region {
  pub fn from_corners<'a,S,T>(lb:T,ub:T) -> Self
  where
    T: IntoIterator<Item=&'a S>,
    S: PartialEq+Copy+ToI64+'a
  {    
    use std::cmp::{min,max};
    use std::mem::uninitialized;
    let mut a=lb.into_iter().map(|v| {v.to_i64()});
    let mut b=ub.into_iter().map(|v| {v.to_i64()});
    let mut mn:[i64;5]=unsafe{uninitialized()};
    let mut mx:[i64;5]=unsafe{uninitialized()};
    for i in 0..5 {
      let a=a.next().unwrap_or(0);
      let b=b.next().unwrap_or(0);
      mn[i]=min(a,b);
      mx[i]=max(a,b);       
    }
    Region{min:mn,max:mx}
  }

  fn dims(&self) -> [u64;5] {
    use std::mem::uninitialized;    
    let mut d:[u64;5]=unsafe{uninitialized()};
    for i in 0..5 {
      d[i]=(self.max[i]-self.min[i]) as u64;
    }
    d
  }

  fn nelem(&self)->u64 {
    (&self.dims()).into_iter().fold(1,|n,v|{n*v})
  }

  fn translate(mut self,ori:&[i64])->Self {
    let mut o=ori.into_iter();
    for i in 0..5 {
      let delta:i64=*o.next().unwrap_or(&0);
      self.min[i]+=delta;
      self.max[i]+=delta;
    }
    self
  }

  fn clip(&self,r:&Region)->Region {
    use std::mem::uninitialized;
    use std::cmp::{min,max};
    let mut out:Region=unsafe{uninitialized()};
    for i in 0..5 {
      out.max[i]=min(self.max[i],r.max[i]);
      out.min[i]=min(out.max[i],max(self.min[i],r.min[i]));
    }
    out
  }

  fn is_empty(&self)->bool {
    for i in 0..5 {
      if self.max[i]==self.min[i] {
        return true;
      }
    }
    false
  }
}

#[test]
fn nelem() {    
  let r=Region{min:[0,0,0,0,0],max:[5,5,5,1,1]};
  assert_eq!(r.nelem(),125);
}

#[test]
fn clip() {
  let r=Region::from(&[5,5,5]);
  // out of bounds and too big. unequal dims.
  let r2=r.clip(&Region::from(&[16]).translate(&[1,8]));
  assert_eq!(r2,Region{min:[1,5,0,0,0],max:[5,5,1,1,1]});
  assert_eq!(r2.nelem(),0);
  // more reasonable 
  let r2=r.clip(&Region::from(&[3,3,3]).translate(&[1,2,3]));
  assert_eq!(r2,Region{min:[1,2,3,0,0],max:[4,5,5,1,1]});
  assert_eq!(r2.nelem(),18);
}

// I don't think this has to get exposed publicly
// So, just specialized what's requried here.
//
// This is all to support From as a way of building Regions,
// which is kind of useless.  I don't really want to dirty
// client code so much...
pub trait ToI64 {
  fn to_i64(self)->i64;
}

impl ToI64 for u32 { 
  fn to_i64(self)->i64 {self as i64}
}

impl ToI64 for i32 { 
  fn to_i64(self)->i64 {self as i64}
}

impl ToI64 for u64 { 
  fn to_i64(self)->i64 {self as i64}
}

impl ToI64 for i64 { 
  fn to_i64(self)->i64 {self}
}

/// Building Regions
/// 
/// # Example
/// ```
/// use klb::read::Region;
/// let shape:[u64;3]=[64,64,64];
/// let r=Region::from(&shape);
/// ```
impl<'a,S,T> From<T> for Region 
where
  T: IntoIterator<Item=&'a S>,
  S: PartialEq+Copy+ToI64+'a
{
  fn from(shape:T) -> Region {
    // No 0-sized dimensions.  0's are coerced to 1's.
    let mut s=shape.into_iter().map(|v| {let t=v.to_i64(); if t==0 {1i64} else {t}});
    Region{min:[0;5],max:[
      s.next().unwrap_or(1),
      s.next().unwrap_or(1),
      s.next().unwrap_or(1),
      s.next().unwrap_or(1),
      s.next().unwrap_or(1)
    ]}
  }
}

impl<'a> From<&'a KLBVolume> for Region {
  fn from(v:&'a KLBVolume) -> Self {
    Region::from(&v.header.xyzct)
  }
}

trait Order {
  fn to_coord(&self,i:i64)->[i64;5];
  fn to_index(&self,r:[i64;5])->i64;
}

#[derive(Debug,Copy,Clone)]
pub struct Strides {
  region: Region,
  strides: [i64;5]
}

impl Strides {
  fn transpose(mut self, i:usize, j:usize)->Self {
    self.region.min.swap(i,j);
    self.region.max.swap(i,j);
    self.strides.swap(i,j);
    self
  }

  pub fn translate(mut self,ori:&[i64])->Self {
    self.region=self.region.translate(ori);
    self
  }

  pub fn nelem(&self)->usize {
    self.strides[4] as usize
  }
}

impl From<Region> for Strides {
  fn from(r:Region)->Strides {
    use std::mem::uninitialized;
    let mut strides:[i64;5]=unsafe{uninitialized()};
    strides[0]=1;    
    for i in 1..5 {
      strides[i]=strides[i-1]*(r.max[i-1]-r.min[i-1]);
    }
    Strides{region:r,strides}
  }
}

impl Order for Strides {
  fn to_index(&self,r:[i64;5])->i64 {
    let mut i=0;
    for d in 0..5 {
      i+=r[d]*self.strides[d];
    }
    i
  }
  fn to_coord(&self,i:i64)->[i64;5] {
    let mut r:[i64;5]=[0;5];
    let mut remainder=i;
    for d in 0..5 {
      r[4-d]=remainder/self.strides[4-d];
      remainder-=r[4-d]*self.strides[4-d];
    }
    r
  }
}
use std::fmt::Debug;
#[derive(Debug)]
pub struct StridedRef<'a,T:Debug+Sized+'a> {
  pub buf: &'a [T], // points to the voxel at desc.region.min
  pub desc: Strides,
}

#[derive(Debug)]
pub struct StridedRefMut<'a,T:Debug+Sized+'a> {
  pub buf: &'a mut [T], // points to the voxel at desc.region.min
  pub desc: Strides,
}

impl<'a,T:Sized+Debug> StridedRefMut<'a,T> {
  /// Clips and recomputes strides
  /// Moves buf pointer to min voxel
  pub fn clip(self,bounds:&Region)->Self{
    let Region{min:oldmin,max:_}=self.desc.region;
    let r=bounds.clip(&self.desc.region);
    let s=Strides::from(r);
    if r.is_empty() {
      // Don't do anything to buf, but return the empty region.
      StridedRefMut{buf:self.buf,desc:s}
    } else {      
      let mut delta=0;
      for d in 0..5 {
        // 1. The new region is inside the old one
        //    so new min-old min should be positive
        // 2. Use the new strides bc those are the "true" ones for the region
        //    The pre-clipped shape is assumed to be invalid.
        delta+=(s.region.min[d]-oldmin[d])*s.strides[d];
      }
      let (_,rest)=self.buf.split_at_mut(delta as usize);
      StridedRefMut{buf:rest,desc:s}
    }
  }
}

impl<'a,T:Sized+Debug> StridedRef<'a,T> {

  /// Clips and recomputes strides
  /// Moves buf pointer to min voxel
  pub fn clip(self,bounds:&Region)->Self{
    let Region{min:oldmin,max:_}=self.desc.region;
    let r=bounds.clip(&self.desc.region);
    let s=Strides::from(r);
    if r.is_empty() {
      // Don't do anything to buf, but return the empty region.
      StridedRef{buf:self.buf,desc:s}
    } else {      
      let mut delta=0;
      for d in 0..5 {
        // 1. The new region is inside the old one
        //    so new min-old min should be positive
        // 2. Use the new strides bc those are the "true" ones for the region
        //    The pre-clipped shape is assumed to be invalid.
        delta+=(s.region.min[d]-oldmin[d])*s.strides[d];
      }
      let (_,rest)=self.buf.split_at(delta as usize);
      StridedRef{buf:rest,desc:s}
    }
  }

  /// assumes 
  ///   min's are inclusive
  ///   max's are exclusive
  ///   pointers point to the voxel at min for respective regions
  pub fn copy(dst: &mut StridedRefMut<'a,T>, src: &Self) {    
    use std::ptr::copy_nonoverlapping;
    let region=dst.desc.region.clip(&src.desc.region);
    if region.is_empty() {// no intersection, so no copy
      return;
    }
    let Region{min,max}=region;
    let nx=(max[0]-min[0]) as usize;
    // compute offsets for min coord and adjust for min x.
    // this is a static offset, so this gets done outside the loop
    let mut isrc0=0;
    let mut idst0=0;
    for d in 0..5 {
      isrc0+=src.desc.region.min[d]*src.desc.strides[d];
      idst0+=dst.desc.region.min[d]*dst.desc.strides[d];
    }
    isrc0=min[0]-isrc0; // subtract off all dims but x, because we'll add them back
    idst0=min[0]-idst0; // this is bc we iterate from min[d]...max[d] below
    // iterate over voxels in the intersection volume
    for t in min[4]..max[4] {
      let isrc4=isrc0+t*src.desc.strides[4];
      let idst4=idst0+t*dst.desc.strides[4];
      for c in min[3]..max[3] {      
        let isrc3=isrc4+c*src.desc.strides[3];
        let idst3=idst4+c*dst.desc.strides[3];
        for z in min[2]..max[2] {
          let isrc2=isrc3+z*src.desc.strides[2];
          let idst2=idst3+z*dst.desc.strides[2];
          for y in min[1]..max[1] {                  
            let isrc=isrc2+y*src.desc.strides[1];
            let idst=idst2+y*dst.desc.strides[1];
            unsafe{copy_nonoverlapping(&src.buf[isrc as usize],&mut dst.buf[idst as usize],nx)};            
          }
        }
      }
    } // end of all the for loops
  } // end of copy fn
}

#[test]
fn copy_transpose() {
  let original=[
     1, 2, 5, 6,
     3, 4, 7, 8,
     9,10,13,14,
    11,12,15,16
  ];
  let expected=[
     1, 2, 3, 4,
     5, 6, 7, 8,
     9,10,11,12,
    13,14,15,16
  ];
  let mut b=[0;16];
  let va=StridedRef{
    buf:&original,
    desc:Strides::from(Region::from(&[2,2,2,2])).transpose(1,2)
  };
  
  {
    let mut vb=StridedRefMut{
      buf:&mut b,
      desc:Strides::from(Region::from(&[2,2,2,2]))
    };    
    StridedRef::copy(&mut vb,&va);
  }
  assert_eq!(expected,b);
}

#[test]
fn copy_min_bounds() {
  let mut b=[
     1, 2, 5, 6,
     3, 4, 7, 8,
     9,10,13,14,
    11,12,15,16
  ];  
  let a=[17;6];
  let expected=[
    17, 2, 5, 6,
    17, 4, 7, 8,
     9,10,13,14,
    11,12,15,16
  ];
  {
    let va=StridedRef{
      buf:&a,
      desc:Strides::from(Region::from(&[2,3]).translate(&[-1,-1]))
    };
    let mut vb=StridedRefMut{
      buf:&mut b,
      desc:Strides::from(Region::from(&[4,4]))
    };
    StridedRef::copy(&mut vb,&va);
  }
  assert_eq!(expected,b);
}

#[test]
fn copy_inner_bounds() {
  let mut b=[
     1, 2, 5, 6,
     3, 4, 7, 8,
     9,10,13,14,
    11,12,15,16
  ];  
  let a=[17;6];
  let expected=[
     1, 2, 5, 6,
     3,17,17, 8,
     9,17,17,14,
    11,17,17,16
  ];
  {
    let va=StridedRef{
      buf:&a,
      desc:Strides::from(Region::from(&[2,3]).translate(&[1,1]))
    };
    let mut vb=StridedRefMut{
      buf:&mut b,
      desc:Strides::from(Region::from(&[4,4]))
    };
    StridedRef::copy(&mut vb,&va);
  }
  assert_eq!(expected,b);
}

#[test]
fn copy_no_intersection() {
  let mut b=[
     1, 2, 5, 6,
     3, 4, 7, 8,
     9,10,13,14,
    11,12,15,16
  ];  
  let a=[17;6];
  let expected=[
     1, 2, 5, 6,
     3, 4, 7, 8,
     9,10,13,14,
    11,12,15,16
  ];
  {
    let va=StridedRef{
      buf:&a,
      desc:Strides::from(Region::from(&[2,3]).translate(&[11,11]))
    };
    let mut vb=StridedRefMut{
      buf:&mut b,
      desc:Strides::from(Region::from(&[4,4]))
    };
    StridedRef::copy(&mut vb,&va);
  }
  assert_eq!(expected,b);
}

pub enum VariantVec {
  UInt8 (Vec<u8>),
  UInt16(Vec<u16>),
  UInt32(Vec<u32>),
  UInt64(Vec<u64>),
  Int8 (Vec<i8>),
  Int16(Vec<i16>),
  Int32(Vec<i32>),
  Int64(Vec<i64>),
  Float32(Vec<f32>),
  Float64(Vec<f64>),
}

impl From<Vec<u8 >> for VariantVec { fn from(v: Vec<u8 >)->Self{VariantVec::UInt8(v)}} 
impl From<Vec<u16>> for VariantVec { fn from(v: Vec<u16>)->Self{VariantVec::UInt16(v)}} 
impl From<Vec<u32>> for VariantVec { fn from(v: Vec<u32>)->Self{VariantVec::UInt32(v)}} 
impl From<Vec<u64>> for VariantVec { fn from(v: Vec<u64>)->Self{VariantVec::UInt64(v)}} 
impl From<Vec<i8 >> for VariantVec { fn from(v: Vec<i8 >)->Self{VariantVec::Int8(v)}} 
impl From<Vec<i16>> for VariantVec { fn from(v: Vec<i16>)->Self{VariantVec::Int16(v)}} 
impl From<Vec<i32>> for VariantVec { fn from(v: Vec<i32>)->Self{VariantVec::Int32(v)}} 
impl From<Vec<i64>> for VariantVec { fn from(v: Vec<i64>)->Self{VariantVec::Int64(v)}} 
impl From<Vec<f32>> for VariantVec { fn from(v: Vec<f32>)->Self{VariantVec::Float32(v)}} 
impl From<Vec<f64>> for VariantVec { fn from(v: Vec<f64>)->Self{VariantVec::Float64(v)}} 

macro_rules! read_variant {
  ($vol:ident,$type:ty) => ({
    let mut v:Vec<$type>=Vec::with_capacity($vol.nelem());
    unsafe {          
      v.set_len($vol.nelem());
      let mut buf=from_raw_parts_mut(
        &mut v[0] as *mut $type as *mut u8,
        $vol.nbytes()
      );
      $vol.read(&mut buf)?;
    }
    Ok(VariantVec::from(v))
  })
}

/// Volume source
pub trait VolumeSource {
  fn nbytes(&self)->usize;
  fn scalar_type(&self)->ScalarType;  

  /// Reads data from the specified region into a buffer.
  fn read<'a>(&'a self,buf:&mut [u8])->Result<(),Error>;

  fn read_to_vec(&self)->Result<Vec<u8>,Error> {
    let mut buf:Vec<u8>=Vec::with_capacity(self.nbytes());
    unsafe{buf.set_len(self.nbytes())};
    self.read(&mut buf)?;
    Ok(buf)
  }

  fn nelem(&self)->usize {
    return self.nbytes()/self.scalar_type().size();
  }

  fn read_to_variant_vec(&self)->Result<VariantVec,Error> {
    use std::slice::from_raw_parts_mut;
    match self.scalar_type() {
      ScalarType::UInt8   => {read_variant!(self,u8)},
      ScalarType::UInt16  => {read_variant!(self,u16)},
      ScalarType::UInt32  => {read_variant!(self,u32)},
      ScalarType::UInt64  => {read_variant!(self,u64)},
      ScalarType::Int8    => {read_variant!(self,i8)},
      ScalarType::Int16   => {read_variant!(self,i16)},
      ScalarType::Int32   => {read_variant!(self,i32)},
      ScalarType::Int64   => {read_variant!(self,i64)},
      ScalarType::Float32 => {read_variant!(self,f32)},
      ScalarType::Float64 => {read_variant!(self,f64)},
    }
  }
}

pub struct ReaderBuilder {
  nthreads: usize
}

pub fn with_nthreads(nthreads:usize)->ReaderBuilder {
  ReaderBuilder{nthreads}
}

impl ReaderBuilder {
  pub fn open(&self,filename:&str)->Result<KLBVolume,Error> {
    open(filename).and_then(|mut e| {
      e.nthreads=self.nthreads;
      Ok(e)
    })
  }
}

// offsets point 1 passed the last element
#[derive(Debug)]
pub struct KLBVolume {
  file: super::io::ReadableFile,
  pub header: HeaderV0,
  pub offsets: Vec<u64>,
  nthreads: usize
}

struct Interval {
  offset: u64,
  len: u64
}

#[derive(Debug)]
pub struct KLBSubVolume<'a> {
  source: &'a KLBVolume,
  pub region: Region
}


impl<'a> From<&'a KLBVolume> for KLBSubVolume<'a> {
  fn from(source: &'a KLBVolume) -> KLBSubVolume<'a> {
    KLBSubVolume{source,region:source.into()}
  }
}

#[derive(Copy,Clone)]
pub struct BlockLattice {
  strides: Strides,
  block_shape: [u32;5]
}

impl<'a> From<&'a HeaderV0> for BlockLattice {
  fn from(header:&HeaderV0)->BlockLattice {
      //compute volume size in units of blocks (tile dims)       
      let mut dims:[i64;5]=unsafe{uninitialized()};
      let bshape=header.block_shape;
      let vshape=header.xyzct;
      for d in 0..5 {
        dims[d]=((vshape[d]+bshape[d]-1)/bshape[d]) as i64; // ceil
      }
      BlockLattice{
        strides: Strides::from(Region::from(&dims)),
        block_shape: bshape
      }
  }
}

impl BlockLattice {
  pub fn to_coord_px(&self,iblock:usize)->[i64;5] {
    let mut r=self.strides.to_coord(iblock as i64);
    for i in 0..5  {
      r[i]*=self.block_shape[i] as i64;
    }
    r
  }
  pub fn nelem(&self)->usize{
    self.strides.nelem()
  }
}

pub fn open(filename:&str)->Result<KLBVolume,Error> {
    use super::io::Reader;
    use super::io::ReadableFile;
    let file=ReadableFile::open(filename)?;
    let header = HeaderV0::read(&file)?;
    let offsets = file.read_vec(header.nblocks(), HeaderV0::raw_size() as u64)?;
    Ok(KLBVolume{file,header,offsets,nthreads:num_cpus::get()*2})
  }

impl<'a> KLBVolume {  
  pub fn roi(&'a self,r:Region) -> KLBSubVolume<'a> {
    KLBSubVolume{source:self,region:Region::from(self).clip(&r)}
  }

  fn block_interval(&self, i:usize)->Interval {
    use std::mem::size_of;
    // offsets point 1 passed the last element  
    let os=self.offsets.as_slice();
    let hdr = (HeaderV0::raw_size() + (os.len() * size_of::<u64>())) as u64;
    let prev = if i > 0 {os[i - 1]} else {0};    
    Interval{offset:prev + hdr,len:os[i] - prev} 
  }

  fn max_block_nbytes(&self)-> u64 {
    use std::cmp::max;
    let (mx,_)=self.offsets.iter().fold((0,0),|(acc,prev),&e|{
      (max(acc,e-prev),e)
    });
    mx
  }

  fn read_block(&self,iblock:usize,buf:&mut Vec<u8>)->Result<(),Error> {
    use io::Reader;
    let interval=self.block_interval(iblock);
    // assert!(buf.capacity()>=interval.len as usize); // FIXME: get rid of this later. should be true bc of allocation in generic_read()
    unsafe{buf.set_len(interval.len as usize)};
    self.file.read_raw(buf,interval.offset)?;
    Ok(())
  }

  /// TODO: get rid of this function
  fn block_lattice(&self)->BlockLattice {
    BlockLattice::from(&self.header)
  }

  /// TODO: get rid of this function
  fn block_coord(&self,block_lattice:&BlockLattice,iblock:usize)->[i64;5]{    
    block_lattice.to_coord_px(iblock)
  }

}

impl VolumeSource for KLBVolume {
  fn nbytes(&self) -> usize {
    Region::from(self).nelem() as usize*self.header.data_type.size()
  }
  
  fn read<'a>(&'a self,buf:&mut [u8])->Result<(),Error> {
    KLBSubVolume::from(self).read(buf)
  }

  fn scalar_type(&self)->ScalarType {
    self.header.data_type
  }
}

unsafe fn make_uninitialized_vec<T:Sized>(n:usize)->Vec<T> {
  let mut v=Vec::with_capacity(n);
  v.set_len(n);
  v
}

impl<'a> VolumeSource for KLBSubVolume<'a> {
  fn nbytes(&self) -> usize {
    self.region.nelem() as usize*self.source.header.data_type.size()
  }

  fn scalar_type(&self)->ScalarType {    
    self.source.header.data_type
  }
  
  fn read(&self,buf:&mut [u8])->Result<(),Error> {    
    fn cast<T>(p:&mut [u8])->&mut [T] {
      use std::mem::size_of;
      use std::slice::from_raw_parts_mut;
      unsafe{from_raw_parts_mut(&p[0] as *const u8 as *mut T,p.len()/size_of::<T>())}
    }

    match self.source.header.data_type {
      ScalarType::UInt8 =>   {let buf:&mut [u8 ]=cast(buf); self.read_generic(buf)},
      ScalarType::UInt16 =>  {let buf:&mut [u16]=cast(buf); self.read_generic(buf)},
      ScalarType::UInt32 =>  {let buf:&mut [u32]=cast(buf); self.read_generic(buf)},
      ScalarType::UInt64 =>  {let buf:&mut [u64]=cast(buf); self.read_generic(buf)},
      ScalarType::Int8 =>    {let buf:&mut [i8 ]=cast(buf); self.read_generic(buf)},
      ScalarType::Int16 =>   {let buf:&mut [i16]=cast(buf); self.read_generic(buf)},
      ScalarType::Int32 =>   {let buf:&mut [i32]=cast(buf); self.read_generic(buf)},
      ScalarType::Int64 =>   {let buf:&mut [i64]=cast(buf); self.read_generic(buf)},
      ScalarType::Float32 => {let buf:&mut [f32]=cast(buf); self.read_generic(buf)},
      ScalarType::Float64 => {let buf:&mut [f64]=cast(buf); self.read_generic(buf)},
    }
  }
}

// rust is such a pleasure
struct EverythingIsFine<T>{ptr:*mut T,len:usize}
unsafe impl<T> Sync for EverythingIsFine<T>{}
impl<T> EverythingIsFine<T> {
  fn new(buf:&mut [T])->Self {
    EverythingIsFine{ptr:&mut buf[0],len:buf.len()}
  }
  fn as_slice_mut(&self)->&mut [T] {
    use std::slice::from_raw_parts_mut;
    unsafe{from_raw_parts_mut(self.ptr,self.len)}
  }
}

impl<'a> KLBSubVolume<'a> {

  fn read_generic<T:Debug>(&self,buf:&mut [T])->Result<(),Error> {
    use std::sync::atomic::{AtomicUsize,Ordering};
    use std;
    use std::sync::{Barrier,Arc};
    use std::sync::mpsc::channel;

    let block_lattice=self.source.block_lattice();
    let block_strides=Strides::from(Region::from(&self.source.header.block_shape));
    let full_dst_layout=Strides::from(self.region);
    let nblocks=self.source.header.nblocks();

    let iblock__=AtomicUsize::new(0);
    // Deal with Rust being pedantic
    let buf_=&EverythingIsFine::new(buf);
    let (tx,rx)=channel::<Error>();
    crossbeam::scope(|scope| {
      let barrier=Arc::new(Barrier::new(self.source.nthreads));
      let iblock_=&iblock__;      
      for _ in 0..self.source.nthreads {
        let barrier=barrier.clone();
        let tx=tx.clone();
        scope.spawn(move || {          
          // setup - per thread resources    
          let mut decoder = self.source.header.compression_type.to_decoder();
          let (mut encoded,mut decoded,mut packet):(Vec<u8>,Vec<u8>,Vec<u8>);
          {
            let nbytes=self.source.header.block_nbytes();
            encoded=Vec::with_capacity(self.source.max_block_nbytes() as usize);
            decoded=Vec::with_capacity(nbytes);
            packet=unsafe{make_uninitialized_vec(nbytes)};
          }
          // make sure all the threads are ready to go
          // before moving on so we're not wildly out of
          // sync later
          barrier.wait(); 
          // Loop over blocks          
          'block_processing: loop { 
            let iblock=iblock_.fetch_add(1,Ordering::AcqRel);
            if iblock>=nblocks {
              break 'block_processing;
            }
            // println!("READ: block {} - read encoded",iblock);
            if let Err(e)=self.source.read_block(iblock,&mut encoded) {
              tx.send(e.into()).unwrap();
              break 'block_processing;
            }
            
            // decode block
            // println!("READ: block {} - decode - input {}",iblock,encoded.len());
            let mut stream=decoder.bind(&encoded);
            'decode: loop {
              match stream.decode(&mut packet) {
                Ok(None) => break 'decode,
                Ok(Some(p)) => {
                  // println!("READ: block {} - packet {}",iblock,p.len());
                  decoded.extend_from_slice(p)// TODO: replace with the ability to do an nd-copy packetwise.
                },
                Err(_) => {
                  // println!("READ: block {} - decomression failure",iblock);
                  tx.send(Error::DecompressFailure).unwrap();
                  break 'block_processing;
                }   
              }
            }
            // copy decoded into output volume
            // println!("READ: block {} - copy",iblock);
            {              
              let block=block_strides
                  .translate(&self.source.block_coord(&block_lattice,iblock));                  
              let vsrc:StridedRef<T>=StridedRef{
                buf: unsafe{std::slice::from_raw_parts(&decoded[0] as *const u8 as *const T,decoded.len()/std::mem::size_of::<T>())},
                desc:block,
              }.clip(&full_dst_layout.region);
              let buf=buf_.as_slice_mut();
              let mut vdst=StridedRefMut{
                buf,
                desc: full_dst_layout,
              };
              StridedRef::copy(&mut vdst,&vsrc);
            }

            // recycle decoded vec
            unsafe {decoded.set_len(0)}; // using set_len instead of safe decode.clear() means inner values aren't explicitly dropped                  
          } // end 'block_processing
        });
      }
    });
    drop(tx);
    if let Some(err)=rx.recv().iter().next() {
      Err(err.clone())
    } else {
      Ok(())
    }
  }
}



// FIXME: rename "strides" to a kind of "Memory Layout"
// FIXME: refactor names (KLBVolume to Reader or something etc.)