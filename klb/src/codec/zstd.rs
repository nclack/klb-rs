extern crate zstd;

use super::{Encoder,Decoder,CodecResult,Stream};
use self::zstd::block::{compress_to_buffer,decompress_to_buffer};

pub struct ZStdEncoder{ level: i32 }
pub struct ZStdDecoder;

impl ZStdEncoder {
    pub fn new()->Self {
        ZStdEncoder{level:0}  // 0 will use zstandard's default (currently 3)
    }
}

impl ZStdDecoder {
    pub fn new()->Self {
        ZStdDecoder{}
    }
}

impl Decoder for ZStdDecoder {
    fn decode<'a,'b>(&mut self,s:&mut Stream<'a>,packet:&'b mut [u8])->CodecResult<'b> {
        // use self::zstd::stream::Decoder as Z;
        // use std::io::Read;
        // let mut d=Z::new(s)?;
        // d.read(packet)?;
        // Ok(Some(packet))

        if let Some(input)=s.input {
            let nbytes=decompress_to_buffer(&input,packet)?;
            s.input=Option::None;
            let (output,_)=packet.split_at_mut(nbytes);
            Ok(Some(output))
        } else {
            Ok(None)
        }
    }
}

impl Encoder for ZStdEncoder {
    fn encode<'a,'b>(&mut self,s:&mut Stream<'a>,packet:&'b mut [u8])->CodecResult<'b> {        
        if let Some(input)=s.input {
            let nbytes=compress_to_buffer(&input,packet,self.level)?;
            s.input=Option::None;
            let (output,_)=packet.split_at_mut(nbytes);
            Ok(Some(output))
        } else {
            Ok(None)
        }
    }
} 

#[test]
// #[ignore] // expect a panic bc garbage data
fn decode_example() {
    let encoded=[0;512];    
    let mut ctx=ZStdDecoder::new();
    let mut stream=(&mut ctx as &mut Decoder).bind(&encoded);
    let mut packet:Vec<u8>=Vec::with_capacity(3200);
    unsafe{packet.set_len(3200)};
    let mut error_detected=false;
    'outer: loop {        
        match stream.decode(&mut packet) {
            Ok(Some(p)) => println!("Packet len {}.",p.len()),
            Ok(None) => break 'outer,
            Err(_) => { error_detected=true; break 'outer; }
        }
    }
    assert_eq!(error_detected,true);
}

#[test]
fn encode_example() {
    let decoded=[0;512];    
    let mut ctx=ZStdEncoder::new();
    let mut stream=(&mut ctx as &mut Encoder).bind(&decoded);
    let mut packet:Vec<u8>=Vec::with_capacity(32);
    unsafe{packet.set_len(30)};
    let mut i=0;
    'outer: loop {        
        match stream.encode(&mut packet) {
            Ok(Some(p)) => println!("{}: Packet len {}.",i,p.len()),
            Ok(None) => break 'outer,
            Err(e) => panic!("{:?}",e)
        }
        i=i+1;
    }
}

#[test]
fn codec_identity() {
    let original:Vec<u8>=(0..127).collect();
    let mut encoded=Vec::new();
    let mut decoded=Vec::new();
    
    let mut packet:Vec<u8>=Vec::with_capacity(470);
    unsafe{packet.set_len(470)};

    let mut encoder=ZStdEncoder::new();    
    let mut stream=(&mut encoder as &mut Encoder).bind(&original);
    'encode: loop {        
        match stream.encode(&mut packet) {
            Ok(Some(p)) => encoded.extend_from_slice(p),
            Ok(None) => break 'encode,
            Err(e) => panic!("{:?}",e)
        }
    }

    let mut decoder=ZStdDecoder::new();
    let mut stream=(&mut decoder as &mut Decoder).bind(&encoded);
    'decode: loop {        
        match stream.decode(&mut packet) {
            Ok(Some(p)) => decoded.extend_from_slice(p),
            Ok(None) => break 'decode,
            Err(e) => panic!("{:?}",e)
        }
    }

    println!("original: {} bytes   decoded: {} bytes",original.len(),decoded.len());
    assert_eq!(original.len(),decoded.len());
    for (a,b) in original.iter().zip(&decoded) {
        assert_eq!(a,b);
    }
}