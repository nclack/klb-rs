use super::{Encoder,Decoder,CodecResult,Stream};

pub struct Uncompressed;

impl Uncompressed {
    pub fn new()->Self {
        Uncompressed{}
    }
}

impl Decoder for Uncompressed {
    fn decode<'a,'b>(&mut self,input:&mut Stream<'a>,packet:&'b mut [u8])->CodecResult<'b> {
        if let Some(encoded)=input.input {
            let used=if packet.len()<encoded.len() {
                let (used,rest)=encoded.split_at(packet.len());
                input.input=Some(rest);
                used                
            } else {
                input.input=Option::None;
                encoded                
            };
            let (output,_)=packet.split_at_mut(used.len());
            output.clone_from_slice(used); // this is the real "decoding" stop.  For the "Uncompressed" decoder, this is just a copy
            Ok(Some(output))
        } else {
            Ok(None)
        }
    }
}

impl Encoder for Uncompressed {
    fn encode<'a,'b>(&mut self,input:&mut Stream<'a>,packet:&'b mut [u8])->CodecResult<'b> {        
        if let Some(encoded)=input.input {
            let used=if packet.len()<encoded.len() {
                let (used,rest)=encoded.split_at(packet.len());
                input.input=Some(rest);
                used                
            } else {
                input.input=Option::None;
                encoded                
            };
            let (output,_)=packet.split_at_mut(used.len());
            output.clone_from_slice(used); // this is the real "encoding" stop.  For the "Uncompressed" encoder, this is just a copy
            Ok(Some(output))
        } else {
            Ok(None)
        }
    }
} 

#[test]
fn decode_example() {
    let encoded=[0;512];    
    let mut ctx=Uncompressed::new();
    let mut stream=(&mut ctx as &mut Decoder).bind(&encoded);
    let mut packet:Vec<u8>=Vec::with_capacity(32);
    unsafe{packet.set_len(32)};
    let mut i=0;
    'outer: loop {        
        match stream.decode(&mut packet) {
            Ok(Some(p)) => println!("{}: Packet len {}.",i,p.len()),
            Ok(None) => break 'outer,
            Err(e) => panic!("{:?}",e)
        }
        i=i+1;
    }
}

#[test]
fn encode_example() {
    let decoded=[0;512];    
    let mut ctx=Uncompressed::new();
    let mut stream=(&mut ctx as &mut Encoder).bind(&decoded);
    let mut packet:Vec<u8>=Vec::with_capacity(32);
    unsafe{packet.set_len(30)};
    let mut i=0;
    'outer: loop {        
        match stream.encode(&mut packet) {
            Ok(Some(p)) => println!("{}: Packet len {}.",i,p.len()),
            Ok(None) => break 'outer,
            Err(e) => panic!("{:?}",e)
        }
        i=i+1;
    }
}

#[test]
fn codec_identity() {
    let original:Vec<u8>=(0..127).collect();
    let mut encoded=Vec::new();
    let mut decoded=Vec::new();
    
    let mut packet:Vec<u8>=Vec::with_capacity(47);
    unsafe{packet.set_len(47)};

    let mut encoder=Uncompressed::new();    
    let mut stream=(&mut encoder as &mut Encoder).bind(&original);
    'encode: loop {        
        match stream.encode(&mut packet) {
            Ok(Some(p)) => encoded.extend_from_slice(p),
            Ok(None) => break 'encode,
            Err(e) => panic!("{:?}",e)
        }
    }

    let mut decoder=Uncompressed::new();
    let mut stream=(&mut decoder as &mut Decoder).bind(&encoded);
    'decode: loop {        
        match stream.decode(&mut packet) {
            Ok(Some(p)) => decoded.extend_from_slice(p),
            Ok(None) => break 'decode,
            Err(e) => panic!("{:?}",e)
        }
    }

    println!("original: {} bytes   decoded: {} bytes",original.len(),decoded.len());
    assert_eq!(original.len(),decoded.len());
    for (a,b) in original.iter().zip(&decoded) {
        assert_eq!(a,b);
    }
}