extern crate flate2;

use super::{Decoder,Encoder,CodecResult,CodecError,Stream};
use self::flate2::{Decompress,Flush,DataError,Status,Compress,Compression};

const WITH_HEADER:bool=false;

pub struct ZlibDecoder {
    dec: Decompress,
}

pub struct ZlibEncoder {
    enc: Compress,
}

impl ZlibDecoder {
    pub fn new()->Self {
        ZlibDecoder {
            dec: Decompress::new(WITH_HEADER), // use a zlib header?...
        }
    }
}

impl ZlibEncoder {
    pub fn new()->Self {
        ZlibEncoder {
            enc: Compress::new(Compression::Default,WITH_HEADER)
        }
    }
}

impl From<DataError> for CodecError {
    fn from(_: DataError) -> Self {CodecError::InvalidData}
}

impl Decoder for ZlibDecoder {
    fn decode<'a,'b>(&mut self,input:&mut Stream<'a>,packet:&'b mut [u8])->CodecResult<'b> {
        if let Some(encoded)=input.input {
            let start=self.dec.total_out();
            match self.dec.decompress(encoded,packet,Flush::None)? {                
                Status::BufError => { 
                    // Nothing happened because packet was too small or not enough input
                    Err(CodecError::InsufficientPacketSize(packet.len()))
                },
                Status::Ok => {
                    // more input needed or more output buffer is full
                    let consumed=self.dec.total_out()-start;
                    let (output,_)=packet.split_at_mut(consumed as usize);
                    Ok(Some(output))
                },
                Status::StreamEnd => { 
                    // all input has been consumed
                    input.input=None;
                    let consumed=self.dec.total_out()-start;
                    let (output,_)=packet.split_at_mut(consumed as usize);
                    self.dec.reset(WITH_HEADER);
                    Ok(Some(output))
                },
            }
        } else {
            Ok(None)
        }
    }
}

impl Encoder for ZlibEncoder {
    fn encode<'a,'b>(&mut self,s:&mut Stream<'a>,packet:&'b mut [u8])->CodecResult<'b> {
        if let Some(input)=s.input {
            let start=self.enc.total_out();
            match self.enc.compress(input,packet,Flush::Finish) { // Fernando's code uses Z_FINISH
                Status::BufError => {
                    Err(CodecError::InsufficientPacketSize(packet.len()))
                },
                Status::Ok => {
                    let consumed=self.enc.total_out()-start;
                    let (output,_)=packet.split_at_mut(consumed as usize);
                    Ok(Some(output))
                },
                Status::StreamEnd => {
                    s.input=None;
                    let consumed=self.enc.total_out()-start;
                    let (output,_)=packet.split_at_mut(consumed as usize);
                    self.enc.reset();
                    Ok(Some(output))
                }
            }
        } else {
            Ok(None)
        }

    }
} 

#[test]
fn encode_example() {
    let original:Vec<u8>=(0..127).collect();
    let mut ctx=ZlibEncoder::new();
    let mut stream=(&mut ctx as &mut Encoder).bind(&original);
    let mut packet:Vec<u8>=Vec::with_capacity(32);
    unsafe{packet.set_len(30)};
    let mut i=0;
    'outer: loop {        
        match stream.encode(&mut packet) {
            Ok(Some(p)) => println!("{}: Packet len {}.",i,p.len()),
            Ok(None) => break 'outer,
            Err(e) => panic!("{:?}",e)
        }
        i=i+1;
    }
}

#[test]
fn codec_identity() {
    let original:Vec<u8>=(0..127).collect();    
    let mut decoded=Vec::new();

    {
        let mut encoded=Vec::new();
        
        let mut packet:Vec<u8>=Vec::with_capacity(47);
        unsafe{packet.set_len(47)};
        let mut encoder=ZlibEncoder::new();
        let mut stream=(&mut encoder as &mut Encoder).bind(&original);
        'encode: loop {                
            match stream.encode(&mut packet) {
                Ok(Some(p)) => encoded.extend_from_slice(p),
                Ok(None) => break 'encode,
                Err(e) => panic!("{:?}",e)
            }
            println!("Encoded {} bytes",encoded.len());
        }

        let mut decoder=ZlibDecoder::new();
        let mut stream=(&mut decoder as &mut Decoder).bind(&encoded);
        'decode: loop {        
            match stream.decode(&mut packet) {
                Ok(Some(p)) => decoded.extend_from_slice(p),
                Ok(None) => break 'decode,
                Err(e) => panic!("{:?}",e)
            }
            println!("Decoded {} bytes",decoded.len())
        }
    }

    println!("original: {} bytes   decoded: {} bytes",original.len(),decoded.len());
    assert_eq!(original.len(),decoded.len());
    for (a,b) in original.iter().zip(&decoded) {
        assert_eq!(a,b);
    }
}

// TODO: what's the right behavior wrt the zlib header

// NOTE: Can assume the input buffer is the entire input