use std::fmt::Display;

#[derive(Debug, Clone, Copy, PartialEq, Eq)]
pub struct BadCompressionIdentifier(u8);

impl BadCompressionIdentifier {
  pub fn id(&self)->u8 {self.0}
}

#[derive(Debug, Clone, Copy, PartialEq, Eq)]
pub enum Identifier {
  None,
  BZip2,
  ZLib,  
  ZStd,
}

impl Identifier {
  pub fn try_from_raw(raw: u8) -> Result<Identifier, BadCompressionIdentifier> {
    match raw {
      0 => Ok(Identifier::None),
      1 => Ok(Identifier::BZip2),
      2 => Ok(Identifier::ZLib),      
      3 => Ok(Identifier::ZStd),
      _ => Err(BadCompressionIdentifier(raw)),
    }
  }

  pub fn to_decoder(&self) -> Box<super::Decoder> {
    use super::{uncompressed,bzip,zlib};
    match self {
      &Identifier::None => {Box::new(uncompressed::Uncompressed::new())},
      &Identifier::BZip2=> {Box::new(bzip::BZip2Decoder::new())},
      &Identifier::ZLib => {Box::new(zlib::ZlibDecoder::new())},      
      &Identifier::ZStd => {Box::new(super::zstd::ZStdDecoder::new())},
    }
  }

  pub fn to_encoder(&self) -> Box<super::Encoder> {
    use super::{uncompressed,bzip,zlib};
    match self {
      &Identifier::None => {Box::new(uncompressed::Uncompressed::new())},
      &Identifier::BZip2=> {Box::new(bzip::BZip2Encoder::new())},
      &Identifier::ZLib => {Box::new(zlib::ZlibEncoder::new())},      
      &Identifier::ZStd => {Box::new(super::zstd::ZStdEncoder::new())},
    }
  }
}

impl From<Identifier> for u8 {
  fn from(t: Identifier) -> u8 {
    match t {
      Identifier::None => 0,
      Identifier::BZip2 => 1,
      Identifier::ZLib => 2,      
      Identifier::ZStd => 3,
    }
  }
}

impl Display for Identifier {
  fn fmt(&self, f: &mut ::std::fmt::Formatter) -> Result<(), ::std::fmt::Error> {
    write!(f,"{}",
      match *self {
        Identifier::None  => "None",
        Identifier::BZip2 => "BZip2",
        Identifier::ZLib  => "ZLib",        
        Identifier::ZStd  => "ZStd",
      }
    )
  }
}

