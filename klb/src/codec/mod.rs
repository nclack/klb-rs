mod identifier;
pub mod uncompressed;
pub mod zlib;
pub mod bzip;
pub mod zstd;

pub use self::identifier::{Identifier,BadCompressionIdentifier};
pub use self::zlib::{ZlibDecoder,ZlibEncoder};
pub use self::bzip::{BZip2Decoder,BZip2Encoder};
pub use self::zstd::{ZStdDecoder,ZStdEncoder};
pub use self::uncompressed::Uncompressed;

#[derive(Debug)]
pub enum CodecError {
    InvalidData,
    InsufficientPacketSize(usize),
    IoError(::std::io::Error)
}

impl<'a> From<::std::io::Error> for CodecError {
    fn from(e: ::std::io::Error)->CodecError {
        CodecError::IoError(e)
    }
}

pub type CodecResult<'a> = Result<Option<&'a mut [u8]>,CodecError>;

pub trait Encoder {
    fn encode<'a,'b>(&mut self,input:&mut Stream<'a>,packet:&'b mut [u8])->CodecResult<'b>;
}

pub trait Decoder {
    fn decode<'a,'b>(&mut self,input:&mut Stream<'a>, packet:&'b mut [u8])->CodecResult<'b>;
}

impl Decoder {
    pub fn bind<'a,'b>(self:&'a mut Self,input:&'b [u8])->BoundDecoder<'a,'b> {
        BoundDecoder::new(self,input)
    }
}

impl Encoder {
    pub fn bind<'a,'b>(self:&'a mut Self,input:&'b [u8])->BoundEncoder<'a,'b> {
        BoundEncoder::new(self,input)
    }
}

pub struct Stream<'a>{
    input: Option<&'a [u8]>
}

// use ::std;
// use std::cmp::min;

// impl<'a> std::io::Read for Stream<'a> {
//     fn read(&mut self,buf:&mut [u8])->std::io::Result<usize> {
//         if let Some(input)=self.input {
//             let n=min(buf.len(),input.len());
//             let (head,rest)=input.split_at(n);
//             self.input=if rest.len()>0 {Some(rest)} else {Option::None};
//             buf.clone_from_slice(head);  // FIXME: avoid copy by swapping contents
//             Ok(n)
//         } else {
//             Ok(0)
//         }
//     }
// }

// impl<'a> std::io::Write for Stream<'a> {
//     fn write(&mut self, buf: &[u8]) -> std::io::Result<usize> {

//     }
//     fn flush(&mut self) -> std::io::Result<()> {
//         self.input=None;
//         Ok(())
//     }
// }

pub struct BoundDecoder<'a,'b> {
    decoder: &'a mut Decoder,
    input: Stream<'b>
}

impl<'a,'x,'y> BoundDecoder<'x,'y> {
    fn new(decoder: &'x mut Decoder,input: &'y [u8])->Self {
        BoundDecoder{decoder,input:Stream{input:Some(input)}}
    }
    pub fn decode<'c>(&'a mut self, packet:&'c mut [u8])->CodecResult<'c> {
            self.decoder.decode(&mut self.input,packet)
    }
}

pub struct BoundEncoder<'a,'b> {
    encoder: &'a mut Encoder,
    input: Stream<'b>
}

impl<'a,'x,'y> BoundEncoder<'x,'y> {
    fn new(encoder: &'x mut Encoder,input: &'y [u8])->Self {
        BoundEncoder{encoder,input:Stream{input:Some(input)}}
    }
    pub fn encode<'c>(&'a mut self, packet:&'c mut [u8])->CodecResult<'c> {
            self.encoder.encode(&mut self.input,packet)
    }
}
