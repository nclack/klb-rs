extern crate bzip2;

use super::{Encoder,Decoder,CodecResult,CodecError,Stream};
use self::bzip2::{Decompress,Compress,Status,Action,Compression};

const SMALL:bool=false;   // if this is true, decompression will be slower but use less memory
const WORK_FACTOR:u32=30; // This is the value used in Fernando's code

pub struct BZip2Decoder {
    dec: Decompress,
}

pub struct BZip2Encoder {
    enc: Compress,
}

impl BZip2Decoder {
    pub fn new()->Self {
        BZip2Decoder {
            dec: Decompress::new(SMALL),
        }
    }
}

impl BZip2Encoder {
    pub fn new()->Self {
        BZip2Encoder {
            enc: Compress::new(Compression::Default,WORK_FACTOR)
        }
    }
}

impl From<self::bzip2::Error> for CodecError {
    fn from(_: self::bzip2::Error) -> Self {CodecError::InvalidData}
}

impl Decoder for BZip2Decoder {
    fn decode<'a, 'b>(&mut self,s:&mut Stream,packet:&'b mut [u8])->CodecResult<'b> {
        if let Some(input)=s.input {
            let start_in=self.dec.total_in();
            let start_out=self.dec.total_out();
            let ret=self.dec.decompress(input,packet)?;
            // println!("Decode: {:?}",ret);
            // println!("\tin: {} out: {} remaining: {}",self.dec.total_in(),self.dec.total_out(), input.len());
            match ret {
                Status::MemNeeded => {
                    Err(CodecError::InsufficientPacketSize(packet.len()))
                },
                Status::StreamEnd => {
                    s.input=None;                    
                    let consumed=self.dec.total_out()-start_out;
                    let (output,_)=packet.split_at_mut(consumed as usize);                    
                    self.dec=Decompress::new(SMALL); // FIXME:  I need to reset the decoder state here, but I suspect doing this causes reallocation of working memory.  Not sure how to do this properly
                    Ok(Some(output))
                },
                _ => {
                    let consumed=self.dec.total_in()-start_in;
                    let (_,rest)=input.split_at(consumed as usize);
                    s.input = if rest.len()>0 {Some(rest)} else {None};
                    let consumed=self.dec.total_out()-start_out;
                    let (output,_)=packet.split_at_mut(consumed as usize);
                    Ok(Some(output))
                }
            }
        } else {
            Ok(None)
        }
    }
}

impl Encoder for BZip2Encoder {
    fn encode<'a,'b>(&mut self,s:&mut Stream<'a>,packet:&'b mut [u8])->CodecResult<'b> {
        if let Some(input)=s.input {
            let start_in=self.enc.total_in();
            let start_out=self.enc.total_out();
            let ret=self.enc.compress(input,packet,Action::Finish)?; // action is finish because we're providing all the data.
            // println!("Encode: {:?}",ret);
            // println!("\tin: {} out: {} remaining: {}",self.enc.total_in(),self.enc.total_out(), input.len());
            match ret {
                Status::MemNeeded => {
                    Err(CodecError::InsufficientPacketSize(packet.len()))
                },
                Status::StreamEnd => {
                    s.input=None;
                    let consumed=self.enc.total_out()-start_out;
                    let (output,_)=packet.split_at_mut(consumed as usize);
                    *self=BZip2Encoder::new(); // FIXME:  I need to reset the encoder state here, but I suspect doing this causes reallocation of working memory.  Not sure how to do this properly
                    Ok(Some(output))
                },
                _ => {
                    let consumed=self.enc.total_in()-start_in;
                    let (_,rest)=input.split_at(consumed as usize);
                    s.input=Some(rest);
                    let consumed=self.enc.total_out()-start_out;
                    let (output,_)=packet.split_at_mut(consumed as usize);
                    Ok(Some(output))
                }
            }
        } else {
            Ok(None)
        }
    }
} 

#[test]
fn encode_example() {
    let original:Vec<u8>=(0..127).collect(); //vec![0;127];
    let mut encoder = BZip2Encoder::new();
    let mut stream=(&mut encoder as &mut Encoder).bind(&original);
    let mut packet:Vec<u8>=Vec::with_capacity(30);
    unsafe{packet.set_len(30)};
    let mut i=0;
    'outer: loop {        
        match stream.encode(&mut packet) {
            Ok(Some(p)) => println!("{}: Packet len {}.",i,p.len()),
            Ok(None) => break 'outer,
            Err(e) => panic!("{:?}",e)
        }
        i=i+1;
    }
}

#[test]
fn codec_identity() {
    let original:Vec<u8>=(0..127).collect();    // vec![0;127]; //
    let mut decoded=Vec::new();

    {
        let mut encoded=Vec::new();
        
        let mut packet:Vec<u8>=Vec::with_capacity(47);
        unsafe{packet.set_len(47)};

        let mut encoder = BZip2Encoder::new();
        let mut stream=(&mut encoder as &mut Encoder).bind(&original);        
        'encode: loop {                
            match stream.encode(&mut packet) {
                Ok(Some(p)) => encoded.extend_from_slice(p),
                Ok(None) => break 'encode,
                Err(e) => panic!("{:?}",e)
            }
            println!("Encoded {} bytes",encoded.len());
        }
        println!("Ready to decode {} bytes.",encoded.len());
        let mut decoder = BZip2Decoder::new();
        let mut stream=(&mut decoder as &mut Decoder).bind(&encoded);
        'decode: for _ in 0..5 {        
            match stream.decode(&mut packet) {
                Ok(Some(p)) => decoded.extend_from_slice(p),
                Ok(None) => break 'decode,
                Err(e) => panic!("{:?}",e)
            }
            println!("Decoded {} bytes",decoded.len())
        }
    }

    println!("original: {}, decoded: {}", original.len(),decoded.len());
    assert_eq!(original.len(),decoded.len());
    for (a,b) in original.iter().zip(&decoded) {
        assert_eq!(a,b);
    }
}

// NOTE: Can assume the input buffer is the entire input