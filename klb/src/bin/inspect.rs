extern crate klb;
extern crate time;

use std::env::args;
use klb::read::VolumeSource;

fn main() {
    let args: Vec<_> = args().collect();
    if args.len() > 1 {
        println!("The first argument is {}", args[1]);
        
        let rdr = klb::read::open(&args[1]).unwrap();
        println!("{:?}",rdr);
        let start = time::precise_time_ns();
        let buf=rdr.read_to_vec().unwrap();
        let end = time::precise_time_ns();
        println!("Reading {:?} Time {} ms", rdr.header.xyzct, (end-start) as f64 / 1000000f64);
        println!("Read {:} bytes.",buf.len());
        println!("{:?}",&buf[0..16]);
    }
}


