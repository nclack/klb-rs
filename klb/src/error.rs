#[derive(Clone, Debug, PartialEq)]
#[allow(dead_code)]
pub enum Error {
  BadCompressionTypeId(u8),
  BadScalarTypeId(u8),
  InvalidHeader(String),
  CouldNotOpenForRead(String),
  CouldNotOpenForWrite(String),
  IOError(String),
  MemError(String),
  BadVersionId(u8),
  CompressFailure,
  DecompressFailure,
  DescriptionIsNotNullTerminated,
  DescriptionIsNotUtf8,
  ErrorMessage(String),
  NotImplemented,
}

impl ::std::error::Error for Error {
  fn description(&self) -> &str {
    match *self {
            Error::BadCompressionTypeId(_) => "Bad Compression Type Id",
            Error::BadScalarTypeId(_) => "Bad Scalar Type Id",
            Error::InvalidHeader(ref s) |
            Error::CouldNotOpenForRead(ref s) |  /*"Could not open for reading"*/
            Error::CouldNotOpenForWrite(ref s) | /*"Could not open for writing"*/
            Error::IOError(ref s) |              /*"Problem with I/O operation: {}"*/
            Error::MemError(ref s) |
            Error::ErrorMessage(ref s) => s.as_str(), /* Misc Error strings */
            Error::BadVersionId(_) => "Format version is not recognized.",
            Error::DescriptionIsNotNullTerminated => "Description is not Null terminated.",
            Error::DescriptionIsNotUtf8 => "Description is not utf-8.",
            Error::NotImplemented => "NOT IMPLEMENTED",
            Error::CompressFailure => "Compression failure",
            Error::DecompressFailure => "Decompression failure",
        }
  }
}

impl From<String> for Error {
  fn from(s:String) -> Error {Error::ErrorMessage(s)}
}

#[doc = "String formatting for KLB errors"]
impl ::std::fmt::Display for Error {
  fn fmt(&self, f: &mut ::std::fmt::Formatter) -> Result<(), ::std::fmt::Error> {
    let bad_id_handler = |f: &mut ::std::fmt::Formatter, id| {
      write!(
        f,
        "KLB Error: {} - Got {}.",
        ::std::error::Error::description(self),
        id
      )
    };
    let default_handler = |f: &mut ::std::fmt::Formatter| {
      write!(f, "KLB Error: {}", ::std::error::Error::description(self))
    };
    match *self {
      Error::BadCompressionTypeId(id) |
      Error::BadScalarTypeId(id) |
      Error::BadVersionId(id) => bad_id_handler(f, id),
      Error::NotImplemented |
      Error::DescriptionIsNotNullTerminated |
      Error::DescriptionIsNotUtf8 |
      Error::CompressFailure |      
      Error::DecompressFailure => default_handler(f),
      Error::InvalidHeader(_) => {
        write!(
          f,
          "KLB Error: Invalid Header - {}",
          ::std::error::Error::description(self)
        )
      },
      Error::ErrorMessage(ref s) |
      Error::CouldNotOpenForRead(ref s) |
      Error::CouldNotOpenForWrite(ref s) |
      Error::IOError(ref s) |
      Error::MemError(ref s) => {
        write!(
          f,
          "KLB Error: {} - {}",
          ::std::error::Error::description(self),
          s
        )
      }
    }
  }
}
