// Usage:
// cargo run|<command> read|write
// Benches either read or write on "bench.klb"
//

extern crate time;
extern crate rand;
extern crate crossbeam;
extern crate klb;

use std::env;
use klb::read::VolumeSource;

enum BenchMode {
    Read,
    Write
}

// lie about vol being mut bc rust is bad
fn build_rand<T:Sized+Sync>(vol: &[T]) {
    extern crate num_cpus;
    use rand::RngCore;
    use std::mem::size_of;
    use std::slice::from_raw_parts_mut;
    let nt = num_cpus::get();
    crossbeam::scope(|scope| for i in 0..nt {
        let n = (vol.len()+nt-1) / nt;
        scope.spawn(move || {
            let mut rng = rand::thread_rng();

            let (_,b)=vol.split_at(i*n);
            let (work,_)=b.split_at(n);

            unsafe {
                let mut buf = from_raw_parts_mut(
                    &work[0] as *const T as *mut T as *mut u8,
                    work.len()*size_of::<T>()
                );            
                rng.fill_bytes(&mut buf);
            }
        });
    });
}

fn main() {    
    let mut args_iter = env::args().skip(1);

    let mode = match args_iter.next() {
        Some(s) => {
            match s.trim().to_lowercase().as_str() {
                "read" => BenchMode::Read,
                "write" => BenchMode::Write,
                _ => panic!("First arg must be read|write"),
            }
        }
        None => panic!("<executable> read|write"),
    };

    let filename="bench.klb";

    match mode {
        BenchMode::Read => {
            println!("reading...");
            {
                let reader=klb::read::open(filename).unwrap();
                let mut tm = time::precise_time_ns();
                reader.read_to_vec().unwrap();            
                tm = time::precise_time_ns() - tm;
                println!("Reading {:?} Time {} ms", reader.header.xyzct, (tm as f32)*1e-6);
            }
        }
        BenchMode::Write => {            
            let n=512u32;
            let mut v=vec![0u16;(n*n*n) as usize]; // u16's are typical use case
            build_rand(&mut v);

            println!("writing random data...");
            {
                let writer=klb::write::options()
                    .bzip()
                    // .zlib()
                    // .zstd()
                    // .uncompressed()
                    // .with_nthreads(nthreads)
                    .with_block_shape([64,64,8,1,1])
                    .open(filename).unwrap();
                let t0=time::precise_time_ns();
                writer.write(&v,[n,n,n,1,1]).unwrap();
                let t1=time::precise_time_ns();
                println!("Wrote {} GB/s",v.len() as f32/((t1-t0) as f32));
            }                        
        }
    }
}
