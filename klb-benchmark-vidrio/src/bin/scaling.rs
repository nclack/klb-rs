extern crate klb;
extern crate time;

use klb::read::VolumeSource;


struct Context {
    vol: klb::read::VariantVec,
    header: klb::header::HeaderV0,
    nbytes: f64
}

fn read(filename:&str,nthreads:usize) -> Context{
    let rdr = klb::read::with_nthreads(nthreads).open(&filename).unwrap();    
    let nbytes=rdr.nbytes() as f64;
    let start = time::precise_time_ns();
    let vol=rdr.read_to_variant_vec().unwrap(); 
    let end = time::precise_time_ns();
    println!("{} threads: Reading {:?} Time {} ms {} MB/s",
            nthreads, rdr.header.xyzct, 
            (end-start) as f64*1e-6,1e3*nbytes/(end-start) as f64);   
    Context {
        vol,
        header: rdr.header,
        nbytes
    }
}

fn write(ctx:&Context,nthreads:usize) {
    let nbytes=ctx.nbytes;
    match ctx.vol {
        klb::read::VariantVec::UInt16(ref vol) => {    
            let writer=klb::write::
                WriterOptions::from(&ctx.header)
                .with_nthreads(nthreads)
                .zstd()
                .open("out.klb")
                .unwrap();
            let start = time::precise_time_ns();
            writer.write(&vol,ctx.header.xyzct).unwrap(); 
            let end = time::precise_time_ns();
            println!("{} threads: Writing {:?} Time {} ms {} MB/s",
                    nthreads, ctx.header.xyzct, 
                    (end-start) as f64*1e-6,1e3*nbytes/(end-start) as f64);
        },
        _ => panic!("Unexpected scalar type.")
    }
}

fn main() {
    if let Some(filename) = std::env::args().nth(1) {
        let ctx=read(&filename,56);
        for n in (1..56/2).map(|x|{x*2}) {
            write(&ctx,n as usize);
        }
    } else {
        println!("expected a filename as an argument.");
    }
}
