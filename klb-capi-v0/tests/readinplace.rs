extern crate libklb;
extern crate libc;
extern crate klb;

use libklb::{
    readKLBstackInPlace,UINT8_TYPE,UINT16_TYPE,
    KLB_DATA_TYPE
};
use std::ffi::CString;

const FILENAME:&'static str="../data/test_read_inplace.klb";

fn write_test_file() {
    let im=vec![57331u16;134*123*94];    
    let h=klb::write::open(FILENAME)
        .expect(&format!("Could not open test file for writing: {}",FILENAME));
    h.write(&im,[134,123,94,1,1])
        .expect(&format!("Error writing to {}",FILENAME));
}

#[test]
fn read_in_place() {
    write_test_file();

    let filename=CString::new(FILENAME).unwrap();
    let mut im=vec![0u16;134*123*94];
    let mut datatype:KLB_DATA_TYPE=UINT8_TYPE;
    
    unsafe {
        let ecode=readKLBstackInPlace(
            filename.as_ptr(),
            &mut im[0] as *mut u16 as *mut libc::c_void,
            &mut datatype as *mut KLB_DATA_TYPE,
            32);
        assert_eq!(ecode,0);
        assert_eq!(im[0],57331);
        assert_eq!(datatype,UINT16_TYPE);
    }
}
/*
pub unsafe extern "C" fn readKLBstackInPlace(
  filename: *const c_char,
  im: *mut c_void,
  dataType: *mut c_int, //KLB_DATA_TYPE
  numThreads: c_int,
) -> c_int 
*/