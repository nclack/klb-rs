extern crate libklb;
extern crate libc;
extern crate klb;

use libklb::{readKLBstack,UINT8_TYPE,NONE,KLB_DATA_TYPE,KLB_COMPRESSION_TYPE};
use std::ffi::CString;
use std::ptr::null;

const FILENAME:&'static str="../data/test_read_stack.klb";

fn write_test_file() {
    let im=vec![57331u16;134*123*94];    
    let h=klb::write::open(FILENAME)
        .expect(&format!("Could not open test file for writing: {}",FILENAME));
    h.write(&im,[134,123,94,1,1])
        .expect(&format!("Error writing to {}",FILENAME));
}

#[test]
fn read_stack() {
    write_test_file();

    let filename=CString::new(FILENAME).unwrap();
    let mut shape=[0;5];    
    let mut block=[0;5];
    let mut pxsize=[0.0;5];
    let mut description=[0;256];
    let mut datatype:KLB_DATA_TYPE=UINT8_TYPE;
    let mut compression:KLB_COMPRESSION_TYPE=NONE;
    
    unsafe {
        let ptr={readKLBstack(
            filename.as_ptr(),
            shape.as_mut_ptr(),
            &mut datatype as *mut KLB_DATA_TYPE,
            32,
            pxsize.as_mut_ptr(),
            block.as_mut_ptr(),
            &mut compression as *mut KLB_COMPRESSION_TYPE,
            description.as_mut_ptr()
        )};
        assert_ne!(ptr,null());
        libc::free(ptr as *mut libc::c_void);
    }
}
/*
pub unsafe extern "C" fn readKLBstack(
  filename: *const c_char,
  xyzct: *mut uint32_t, //sz KLB_DATA_DIMS
  dataType: *mut c_int, //KLB_DATA_TYPE
  numThreads: c_int,
  pixelSize: *mut c_float, //sz KLB_DATA_DIMS
  blockSize: *mut uint32_t, //sz KLB_DATA_DIMS
  compressionType: *mut c_int, //KLB_COMPRESSION_TYPE
  metadata: *mut c_char,
) -> *const c_void 
*/