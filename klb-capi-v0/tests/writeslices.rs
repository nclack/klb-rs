extern crate libklb;
extern crate libc;

use libklb::{writeKLBstackSlices,UINT16_TYPE,BZIP2};
use std::ffi::CString;

#[test]
fn write_slices() {
    let shape=[134,123,3,1,1];
    let block=[64,64,8,1,1];
    let pxsize=[3.0,5.0,7.0,11.0,13.0];
    let slice_1=vec![57331u16;134*123];
    let slice_2=vec![40009u16;134*123];
    let slice_3=vec![30011u16;134*123];
    let slices=[
        slice_1.as_ptr(),
        slice_2.as_ptr(),
        slice_3.as_ptr()];
    let filename=CString::new("../data/test_write_klb_slices.klb").unwrap();
    let description=CString::new("This is an example description").unwrap();

    let ecode=unsafe {writeKLBstackSlices(
        slices.as_ptr() as *const *const libc::c_void,
        filename.as_ptr(),
        shape.as_ptr(),
        UINT16_TYPE,
        32,
        pxsize.as_ptr(),
        block.as_ptr(),
        BZIP2,
        description.as_ptr())};
    assert_eq!(ecode,0);
}
/*
pub unsafe extern "C" fn writeKLBstackSlices(
  im: *const *const c_void, //each slice in seperate address
  filename: *const c_char,
  xyzct: *mut uint32_t, //sz KLB_DATA_DIMS
  dataType: KLB_DATA_TYPE,
  numThreads: c_int,
  pixelSize: *mut c_float, //sz KLB_DATA_DIMS
  blockSize: *mut uint32_t, //sz KLB_DATA_DIMS
  compressionType: KLB_COMPRESSION_TYPE,
  metadata: *mut c_char,
) -> c_int 
*/