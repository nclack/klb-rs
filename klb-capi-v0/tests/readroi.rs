extern crate libklb;
extern crate libc;
extern crate klb;

use libklb::readKLBroiInPlace;
use std::ffi::CString;

const FILENAME:&'static str="../data/test_read_roi.klb";

fn write_test_file() {
    let im=make();
    assert_eq!(1468,im[128+10*134]); // double check
    let h=klb::write::open(FILENAME)
        .expect(&format!("Could not open test file for writing: {}",FILENAME));
    h.write(&im,[134,123,94,1,1])
        .expect(&format!("Error writing to {}",FILENAME));
}

fn make()->Vec<u16> {
    let mut im=vec![0u16;134*123*94];
    for i in 0..im.len() {
        im[i]=i as u16;
    }
    im
}

#[test]
fn read_roi() {
    write_test_file();

    let filename=CString::new(FILENAME).unwrap();
    
    let mut im=vec![57331u16;134*123*94];    
    let mut lb=[128,10,0,0,0];
    let mut ub=[200,20,1,1,1];
    
    unsafe {
        let ecode=readKLBroiInPlace(
            filename.as_ptr(),
            &mut im[0] as *mut u16 as *mut libc::c_void,
            lb.as_mut_ptr(),
            ub.as_mut_ptr(),
            1);

        for i in 0..5 {
            println!("{:?}",im[i]);
        }

        assert_eq!(ecode,0);
        assert_eq!(im[0],1468);
        assert_eq!(lb,[128,10,0,0,0]);
        assert_eq!(ub,[134,20,1,1,1]);
    }
}
/*
pub unsafe extern "C" fn readKLBroiInPlace(
  filename: *const c_char,
  im: *mut c_void,
  xyzctLB: *mut uint32_t, //sz KLB_DATA_DIMS
  xyzctUB: *mut uint32_t, //sz KLB_DATA_DIMS
  numThreads: c_int,
) -> c_int 
*/