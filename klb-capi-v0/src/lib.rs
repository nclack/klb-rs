extern crate libc;
extern crate klb;

use libc::{c_char, uint32_t, c_int, c_float, c_void};
use std::convert::AsMut;
use std::ffi::CStr;
use std::{ptr, slice, panic};
use std::io::{Write, stderr};
use std::marker::Sync;
use std::fmt::Debug;

use klb::error::Error;
use klb::write::HasTypeId;
use klb::header::{RawHeader, HeaderV0, ScalarType};

pub const KLB_DATA_DIMS: usize = 5;
pub const KLB_METADATA_SIZE: usize = 256;

// KLB_DATA_TYPE
// These are only here in case you're interacting with
// the c api via rust (as an rlib). 
//
// When linking the library to c, you'd use the .h file
// to define these constants.
#[allow(non_camel_case_types)]

pub type KLB_DATA_TYPE=c_int;
pub const UINT8_TYPE     : KLB_DATA_TYPE = 0;
pub const UINT16_TYPE    : KLB_DATA_TYPE = 1;
pub const UINT32_TYPE    : KLB_DATA_TYPE = 2;
pub const UINT64_TYPE    : KLB_DATA_TYPE = 3;
pub const INT8_TYPE      : KLB_DATA_TYPE = 4;
pub const INT16_TYPE     : KLB_DATA_TYPE = 5;
pub const INT32_TYPE     : KLB_DATA_TYPE = 6;
pub const INT64_TYPE     : KLB_DATA_TYPE = 7;
pub const FLOAT32_TYPE   : KLB_DATA_TYPE = 8;
pub const FLOAT64_TYPE   : KLB_DATA_TYPE = 9;

// KLB_COMPRESSION_TYPE
// These are only here in case you're interacting with
// the c api via rust (as an rlib). 
//
// When linking the library to c, you'd use the .h file
// to define these constants.
#[allow(non_camel_case_types)]
pub type KLB_COMPRESSION_TYPE=c_int;
pub const NONE  : KLB_COMPRESSION_TYPE = 0;
pub const BZIP2 : KLB_COMPRESSION_TYPE = 1;
pub const ZLIB  : KLB_COMPRESSION_TYPE = 2;
pub const ZSTD  : KLB_COMPRESSION_TYPE = 3;

fn copy_to_array<A, T>(slice: &[T]) -> A
where
  A: Sized + Default + AsMut<[T]>,
  T: Copy,
{
  let mut a = Default::default();
  <A as AsMut<[T]>>::as_mut(&mut a).copy_from_slice(slice);
  a
}

fn safe_copy<T, A>(arr: *const T, len: usize) -> Result<A, ()>
where
  T: Copy + Sized + std::panic::RefUnwindSafe,
  A: Sized + Default + AsMut<[T]>,
{
  match panic::catch_unwind(|| unsafe { slice::from_raw_parts(arr, len) }) {
    Ok(slc) => Ok(copy_to_array(slc)),
    Err(_) => Err(()),
  }
}

fn safe_write_typed<T:Debug>(im: &[T], filename: &str, rawhdr: RawHeader, nthreads: i32) -> Result<(), Error> 
  where T: std::panic::RefUnwindSafe+Sync+HasTypeId
{
  use klb::write::WriterOptions;
  let hdr = HeaderV0::try_from_raw(rawhdr)?;
  let writer=WriterOptions::from(&hdr).with_nthreads(nthreads as usize).open(filename)?;
  match panic::catch_unwind(|| writer.write(im,hdr.xyzct)) {
    Ok(res) => res,
    Err(_) => Err(Error::MemError(
      format!("Panic when writing to {}", filename),
    )),
  }
}

fn safe_write(im: &[u8], filename: &str, rawhdr: RawHeader, nthreads: i32) -> Result<(), Error> {
  use std::mem::size_of;
  use std::slice::from_raw_parts;
  fn cast<T>(a:&[u8])->&[T] { unsafe{
    from_raw_parts(
      &a[0] as *const u8 as *const T,
      a.len()/size_of::<T>()
    )
  }}
  match rawhdr.dt as KLB_DATA_TYPE {
    UINT8_TYPE    => { let im:&[u8 ]=cast(im); safe_write_typed(im,filename,rawhdr,nthreads)}
    UINT16_TYPE   => { let im:&[u16]=cast(im); safe_write_typed(im,filename,rawhdr,nthreads)}
    UINT32_TYPE   => { let im:&[u32]=cast(im); safe_write_typed(im,filename,rawhdr,nthreads)}
    UINT64_TYPE   => { let im:&[u64]=cast(im); safe_write_typed(im,filename,rawhdr,nthreads)}
    INT8_TYPE     => { let im:&[i8 ]=cast(im); safe_write_typed(im,filename,rawhdr,nthreads)}
    INT16_TYPE    => { let im:&[i16]=cast(im); safe_write_typed(im,filename,rawhdr,nthreads)}
    INT32_TYPE    => { let im:&[i32]=cast(im); safe_write_typed(im,filename,rawhdr,nthreads)}
    INT64_TYPE    => { let im:&[i64]=cast(im); safe_write_typed(im,filename,rawhdr,nthreads)}
    FLOAT32_TYPE  => { let im:&[f32]=cast(im); safe_write_typed(im,filename,rawhdr,nthreads)}
    FLOAT64_TYPE  => { let im:&[f64]=cast(im); safe_write_typed(im,filename,rawhdr,nthreads)}
    _ => Err(Error::BadScalarTypeId(rawhdr.dt))
  }
}

#[no_mangle]
#[allow(non_snake_case)]
pub unsafe extern "C" fn writeKLBstack(
  im: *const c_void,
  filename: *const c_char,
  xyzct: *const uint32_t, //sz KLB_DATA_DIMS
  dataType: KLB_DATA_TYPE,
  numThreads: c_int,
  pixelSize: *const c_float, //sz KLB_DATA_DIMS
  blockSize: *const uint32_t, //sz KLB_DATA_DIMS
  compressionType: KLB_COMPRESSION_TYPE,
  metadata: *const c_char,
) -> c_int {
  let mut stderr = stderr();
  let errmsg = "Write Error";

  if im.is_null() || filename.is_null() || xyzct.is_null() || pixelSize.is_null() ||
    blockSize.is_null()
  {
    writeln!(
      &mut stderr,
      "{}: Unacceptable NULL arguments (im, filename, xyzct, pixelSize, dataType, compressionType or blockSize is NULL)",
      errmsg
    ).unwrap();
    return 1;
  }

  let xyzct_arr: [u32; KLB_DATA_DIMS];
  match safe_copy(xyzct, KLB_DATA_DIMS) {
    Ok(a) => xyzct_arr = a,
    Err(_) => {
      writeln!(&mut stderr, "{}: Invalid xyzct buffer size", errmsg).unwrap();
      return 1;
    }
  }

  let bufsz;
  match ScalarType::try_from_raw(dataType as u8) {
    Ok(dt) => bufsz = dt.size() * xyzct_arr.into_iter().product::<u32>() as usize,
    Err(e) => {
      writeln!(&mut stderr, "{}: {}", errmsg, e).unwrap();
      return 1;
    }
  }

  let v: &[u8];
  match panic::catch_unwind(|| slice::from_raw_parts(im as *const u8, bufsz)) {
    Ok(slc) => v = slc,
    Err(_) => {
      writeln!(&mut stderr, "{}: Invalid image buffer size", errmsg).unwrap();
      return 1;
    }
  }

  let mut description = [0u8; KLB_METADATA_SIZE];
  if !metadata.is_null() {
    match panic::catch_unwind(|| slice::from_raw_parts(metadata, KLB_METADATA_SIZE)) {
      Ok(slc) => {
        //convert c_char (i8) to u8 array
        for (i, m) in slc.iter().map(|&m| m as u8).enumerate() {
          description[i] = m;
        }
      }
      Err(_) => {
        writeln!(&mut stderr, "{}: Invalid metadata buffer size", errmsg).unwrap();
        return 1;
      }
    }
  }

  let ps: [f32; KLB_DATA_DIMS];
  match safe_copy(pixelSize, KLB_DATA_DIMS) {
    Ok(p) => ps = p,
    Err(_) => {
      writeln!(&mut stderr, "{}: Invalid pixelSize buffer size", errmsg).unwrap();
      return 1;
    }
  }

  let bs: [u32; KLB_DATA_DIMS];
  match safe_copy(blockSize, KLB_DATA_DIMS) {
    Ok(b) => bs = b,
    Err(_) => {
      writeln!(&mut stderr, "{}: Invalid blockSize buffer size", errmsg).unwrap();
      return 1;
    }
  }

  let cstr_res = CStr::from_ptr(filename).to_str();
  if let Err(e) = cstr_res {
    writeln!(&mut stderr, "{}: {}", errmsg, e).unwrap();
  }


  let filename = cstr_res.unwrap();
  let ret=match safe_write(
    v,
    filename,
    RawHeader {
      ver: 2u8,
      xyzct: xyzct_arr,
      px_sz: ps,
      dt: dataType as u8,
      ct: compressionType as u8,
      desc: description,
      bs: bs,
    },
    numThreads as i32,
  ) {
    Err(e) => {
      writeln!(&mut stderr, "{}: {}", errmsg, e).unwrap();
      match e {
        Error::CouldNotOpenForWrite(_) => 5,
        Error::CompressFailure => 2,
        _ => 1,
      }
    }
    Ok(_) => 0,
  };
  ret
}


/*
brief Same as writeKLBstack but we use a double pointer to have each slice in 
a separate address. xyzct[3] = xyzct[4] = 1 (no time or channel information). 
We assume xyzct[2] = number of slices
*/
#[no_mangle]
#[allow(non_snake_case)]
pub unsafe extern "C" fn writeKLBstackSlices(
  im: *const *const c_void, //each slice in seperate address
  filename: *const c_char,
  xyzct: *const uint32_t, //sz KLB_DATA_DIMS
  dataType: KLB_DATA_TYPE,
  numThreads: c_int,
  pixelSize: *const c_float, //sz KLB_DATA_DIMS
  blockSize: *const uint32_t, //sz KLB_DATA_DIMS
  compressionType: KLB_COMPRESSION_TYPE,
  metadata: *const c_char,
) -> c_int {
  let mut stderr = stderr();
  let errmsg = "Write Slices Error";

  if im.is_null() || xyzct.is_null() {
    writeln!(
      stderr,
      "{}: unacceptable NULL arguments.(im or xyzct is NULL)",
      errmsg
    ).unwrap();
    return 1;
  }

  let xyzct_arr: [u32; KLB_DATA_DIMS];
  match panic::catch_unwind(|| {
    copy_to_array(slice::from_raw_parts(xyzct, KLB_DATA_DIMS))
  }) {
    Ok(xyzct_a) => xyzct_arr = xyzct_a,
    Err(_) => {
      writeln!(stderr, "{}: invalid xyzct size", errmsg).unwrap();
      return 1;
    }
  }

  if xyzct_arr[3] != 1 || xyzct_arr[4] != 1 {
    writeln!(
      stderr,
      "{}: number of channels/time points must be 1",
      errmsg
    ).unwrap();
    return 3;
  }

  let type_size = match ScalarType::try_from_raw(dataType as u8) {
    Ok(st) => st.size(),
    Err(_) => {
      writeln!(stderr, "{}: Invalid Data Type", errmsg).unwrap();
      return 1;
    }
  };

  let slice_sz = (xyzct_arr[0] * xyzct_arr[1]) as usize * type_size;
  let tot_sz = slice_sz * (xyzct_arr[2] as usize);
  let mut v: Vec<u8> = Vec::with_capacity(tot_sz);

  let vv_slc = slice::from_raw_parts(im as *const *const u8, xyzct_arr[2] as usize);

  //append into vector
  for i in 0..(xyzct_arr[2] as usize) {
    match panic::catch_unwind(|| slice::from_raw_parts(vv_slc[i], slice_sz)) {
      Ok(slc) => v.extend_from_slice(slc),
      Err(_) => {
        writeln!(stderr, "{}: Invalid im sizes", errmsg).unwrap();
        return 1;
      }
    }
  }

  writeKLBstack(
    v.as_mut_slice().as_mut_ptr() as *mut c_void,
    filename,
    xyzct,
    dataType,
    numThreads,
    pixelSize,
    blockSize,
    compressionType,
    metadata,
  )
}

/// This utility function is used by several of the c api read functions.
/// It copies data read from the header into several arrays.
fn from_hdr(
  hdr: &klb::header::HeaderV0,
  xyzct: *mut uint32_t,
  dt: *mut c_int,
  ps: *mut c_float,
  bs: *mut uint32_t,
  ct: *mut c_int,
  md: *mut c_char,
) {
  use std::mem::transmute;
  use std::slice::from_raw_parts_mut;
  unsafe {
    if !xyzct.is_null() {
      let xyzct_slc: &mut [uint32_t] = from_raw_parts_mut(xyzct, KLB_DATA_DIMS);
      xyzct_slc.copy_from_slice(&hdr.xyzct[..]);
    }

    if !dt.is_null() {
      ptr::replace(dt, u8::from(hdr.data_type) as c_int);
    }

    if !ps.is_null() {
      let pxsz_slc: &mut [c_float] = from_raw_parts_mut(ps, KLB_DATA_DIMS);
      pxsz_slc.copy_from_slice(&hdr.pixel_size[..]);
    }

    if !bs.is_null() {
      let blcksz_slc: &mut [uint32_t] = from_raw_parts_mut(bs, KLB_DATA_DIMS);
      blcksz_slc.copy_from_slice(&hdr.block_shape[..]);
    }

    if !ct.is_null() {
      ptr::replace(ct, u8::from(hdr.compression_type) as c_int);
    }

    if !md.is_null() {
      let metad_slc: &mut [c_char] = from_raw_parts_mut(md, KLB_METADATA_SIZE);
      let desc=transmute(&hdr.description[..]);
      metad_slc.copy_from_slice(desc);
    }
  }
}

#[no_mangle]
#[allow(dead_code, non_snake_case)]
pub unsafe extern "C" fn readKLBheader(
  filename: *const c_char,
  xyzct: *mut uint32_t, //sz KLB_DATA_DIMS
  dataType: *mut KLB_DATA_TYPE,
  pixelSize: *mut c_float, //sz KLB_DATA_DIMS
  blockSize: *mut uint32_t, //sz KLB_DATA_DIMS
  compressionType: *mut KLB_COMPRESSION_TYPE,
  metadata: *mut c_char,
) -> c_int {
  match panic::catch_unwind(||{ 
    let filename = CStr::from_ptr(filename)
      .to_str()
      .expect("Filename does not appear to be a valid Utf8 string");
    let h=klb::read::open(filename)
      .expect(&format!("Could not open file for read: {}",filename));
    from_hdr(&h.header,xyzct,dataType,pixelSize,blockSize,compressionType,metadata);
  }) {
    Err(e) => {
      let mut stderr = stderr();
      writeln!(&mut stderr,"{}: {}","KLB Error",
        if let Some(estr)=e.downcast_ref::<String>() {
          estr
        } else {
          "Unknown."
        }
      ).unwrap();
      1
    },
    Ok(_) => 0
  }
}

/// Malloced Array
/// 
/// Encapsulates memory allocated using the libc malloc/free functions.
/// This is used by one of the c api read functions which allocates memory
/// for the read volume. 
/// 
/// Drop is implemented for this type so things get deallocated properly
/// during an exception.
struct MallocedArray{ptr:*const u8,len:usize}

impl MallocedArray {
  fn new(nbytes:usize)->Result<Self,usize> {
    let ptr = unsafe { libc::malloc(nbytes) as *const u8};
    if ptr.is_null() {
      Err(nbytes)
    } else {
      Ok(MallocedArray{ptr,len:nbytes})
    }
  }

  fn as_slice_mut(&self)->&mut [u8] {
    unsafe{slice::from_raw_parts_mut(self.ptr as *mut u8,self.len)}
  }

  fn release(mut self)->*const c_void {
    let ptr=self.ptr;
    self.ptr=ptr::null();
    ptr as *const c_void
  }
}

impl Drop for MallocedArray {
  fn drop(&mut self) {        
    unsafe{libc::free(self.ptr as *mut c_void)};
  }
}

#[test]
fn libc_free_null() {  
  unsafe{libc::free(ptr::null_mut())};// freeing a null should be no big deal
}

/*
use dataType in order to know how to cast the returned pointer with the image content.

Returns NULL if there was an issue

All the parameters after numThreads (included) are optional
*/
#[no_mangle]
#[allow(dead_code, non_snake_case)]
pub unsafe extern "C" fn readKLBstack(
  filename: *const c_char,
  xyzct: *mut uint32_t, //sz KLB_DATA_DIMS
  dataType: *mut c_int, //KLB_DATA_TYPE
  numThreads: c_int,
  pixelSize: *mut c_float, //sz KLB_DATA_DIMS
  blockSize: *mut uint32_t, //sz KLB_DATA_DIMS
  compressionType: *mut c_int, //KLB_COMPRESSION_TYPE
  metadata: *mut c_char,
) -> *const c_void {
  use klb::read::VolumeSource;
  match panic::catch_unwind(||{ 
    let filename = CStr::from_ptr(filename)
      .to_str()
      .expect("Filename does not appear to be a valid Utf8 string");
    let h=klb::read::with_nthreads(numThreads as usize)
      .open(filename)
      .expect(&format!("Could not open file for read: {}",filename));
    from_hdr(&h.header,xyzct,dataType,pixelSize,blockSize,compressionType,metadata);
    let buf=MallocedArray::new(h.nbytes())
      .expect(&format!("Could not allocate {} bytes",h.nbytes()));
    h.read(buf.as_slice_mut())
      .expect(&format!("Problem encountered reading file: {}",filename));
    buf
  }) {
    Err(e) => {
      let mut stderr = stderr();
      writeln!(&mut stderr,"{}: {}","KLB Error",
        if let Some(estr)=e.downcast_ref::<String>() {
          estr
        } else {
          "Unknown."
        }
      ).unwrap();
      ptr::null_mut::<c_void>()
    },
    Ok(buf) => buf.release()
  }
}

/*
read image when you have already allocated memory and you want to reuse it
(it could be good to read a time series where images are always the same size). 
You can use readKLBheader to extract all the metadata

Return value is a status/error code:
  0 - ok
  1 - there was some problem

This diverges a bit from Amat's code where there were a varienty of error
code's used.  In practice though client code never relied on interpreting 
these codes.
*/
#[no_mangle]
#[allow(dead_code, non_snake_case)]
pub unsafe extern "C" fn readKLBstackInPlace(
  filename: *const c_char,
  im: *mut c_void,
  dataType: *mut c_int, //KLB_DATA_TYPE
  numThreads: c_int,
) -> c_int {
  use klb::read::VolumeSource;
  use std::ptr::null_mut;

  match panic::catch_unwind(|| {
    if im.is_null() {
      panic!("Destination pointer can not be NULL.".to_owned());
    }
    let filename = CStr::from_ptr(filename)
      .to_str()
      .expect("Filename does not appear to be a valid Utf8 string");
    let h=klb::read::with_nthreads(numThreads as usize)
      .open(filename)
      .expect(&format!("Could not open file for read: {}",filename));
    from_hdr(&h.header,null_mut(),dataType,null_mut(),null_mut(),null_mut(),null_mut());
    let buf=slice::from_raw_parts_mut(im as *mut u8,h.nbytes());
    h.read(buf)
      .expect(&format!("Problem encountered reading file: {}",filename));
  }) {
    Err(e) => {
      let mut stderr = stderr();
      writeln!(&mut stderr,"{}: {}","KLB Error",
        if let Some(estr)=e.downcast_ref::<String>() {
          estr
        } else {
          "Unknown."
        }
      ).unwrap();
      1
    },
    Ok(_) => 0
  }
}

/*
read image when you have already allocated memory and you want to reuse it 
(it could be good to read a time series where images are always the same size).
You can use readKLBheader to extract all the metadata

Return value is a status/error code:
  0 - ok
  1 - there was some problem

This diverges a bit from Amat's code where there were a varienty of error
code's used.  In practice though client code never relied on interpreting 
these codes.
*/
#[no_mangle]
#[allow(dead_code, non_snake_case)]
pub unsafe extern "C" fn readKLBroiInPlace(
  filename: *const c_char,
  im: *mut c_void,
  xyzctLB: *mut uint32_t, //sz KLB_DATA_DIMS
  xyzctUB: *mut uint32_t, //sz KLB_DATA_DIMS
  numThreads: c_int,
) -> c_int {
  use klb::read::{VolumeSource,Region};
  use std::slice::{from_raw_parts_mut,from_raw_parts};

  match panic::catch_unwind(|| {
    if im.is_null() {
      panic!("Destination pointer can not be NULL.".to_owned());
    }
    if xyzctLB.is_null() || xyzctUB.is_null() {
      panic!("Pointers to shape arrays can not be NULL.".to_owned());
    }
    let filename = CStr::from_ptr(filename)
      .to_str()
      .expect("Filename does not appear to be a valid Utf8 string");    
    let h=klb::read::with_nthreads(numThreads as usize)
      .open(filename)
      .expect(&format!("Could not open file for read: {}",filename));
    // Make the ROI
    let lb=from_raw_parts(xyzctLB,5);
    let ub=from_raw_parts(xyzctUB,5);
    let h=h.roi(Region::from_corners(lb,ub));
    // read out the clipped shape
    let lb=from_raw_parts_mut(xyzctLB,5);
    let ub=from_raw_parts_mut(xyzctUB,5);
    for i in 0..5 {
      lb[i]=h.region.min[i] as u32;
      ub[i]=h.region.max[i] as u32;
    }
    // read subvolume into the buffer
    let buf=slice::from_raw_parts_mut(im as *mut u8,h.nbytes());
    h.read(buf)
      .expect(&format!("Problem encountered reading file: {}",filename));
  }) {
    Err(e) => {
      let mut stderr = stderr();
      writeln!(&mut stderr,"{}: {}","KLB Error",
        if let Some(estr)=e.downcast_ref::<String>() {
          estr
        } else {
          "Unknown."
        }
      ).expect("Could not write to stderr");
      1
    },
    Ok(_) => 0
  }
}
