extern crate libklb;
extern crate libc;
extern crate time;
extern crate rand;
extern crate crossbeam;


use libklb::writeKLBstack;
use libklb::{KLB_DATA_TYPE,KLB_COMPRESSION_TYPE};
use libklb::{KLB_DATA_DIMS,KLB_METADATA_SIZE};


use libc::{c_char, uint32_t, c_int, c_float};
use std::{mem, slice};
use std::ffi::CString;

// TODO: look into using rayon instead of crossbeam for this.  performance?
fn build_rand(vol: &[u8]) {
    extern crate num_cpus;
    use rand::Rng;
    let nt = if NTHREADS == -1 {
        num_cpus::get()
    } else {
        NTHREADS as usize
    };
    crossbeam::scope(|scope| for i in 0..nt {
        scope.spawn(move || {
            let mut rng = rand::thread_rng();
            let div = vol.len() / nt;
            let off = i * div;

            let len = if i == nt - 1 { vol.len() } else { off + div };

            let buf = vol as *const [u8] as *mut [u8];
            unsafe {
                rng.fill_bytes(&mut (*buf)[off..len]);
            }
        });
    });
}

const NTHREADS: c_int = -1;

fn write<T>(filename: &CString, dt: KLB_DATA_TYPE) {
    let mut xyzct: [uint32_t; KLB_DATA_DIMS] = [512, 512, 512, 1, 1];
    let mut bs: [uint32_t; KLB_DATA_DIMS] = [64, 64, 64, 1, 1];
    let mut ps = [0.0 as c_float; KLB_DATA_DIMS];
    let ct:KLB_COMPRESSION_TYPE = libklb::BZIP2;
    let mut md = [0 as c_char; KLB_METADATA_SIZE];

    println!("allocating...");
    let mut nelem = 1;
    for i in 0..KLB_DATA_DIMS {
        nelem *= xyzct[i] as usize;
    }
    // use malloc for compatibility with C++ code.
    let nbytes = mem::size_of::<T>() * nelem;
    let im = unsafe { libc::malloc(nbytes) };
    if im.is_null() {
        panic!("Error: Insufficient Memory.");
    }
    build_rand(unsafe { slice::from_raw_parts(im as *mut u8, nbytes) });


    let res;
    println!("writing: using {} threads", NTHREADS);
    let mut tm = time::precise_time_ns();
    unsafe {
        res = writeKLBstack(im,
                            filename.as_ptr(),
                            &mut xyzct[0] as *mut uint32_t,
                            dt as c_int,
                            NTHREADS,
                            &mut ps[0] as *mut c_float,
                            &mut bs[0] as *mut uint32_t,
                            ct as c_int,
                            &mut md[0]);
    }
    tm = time::precise_time_ns() - tm;

    if res != 0 {
        panic!("Write Code {}", res);
    }

    println!("Writing {:?} Time {} ms {} MB/s", xyzct, tm as f64 / 1000000f64,tm as f64/nbytes as f64);
}

fn main() {
    let filename = CString::new("bench.klb").unwrap();

    let dt = libklb::UINT8_TYPE;

    match dt { 
        libklb::UINT8_TYPE => write::<u8>(&filename, dt),
        libklb::UINT16_TYPE => write::<u16>(&filename, dt),
        _ => unimplemented!(),
    }
}
