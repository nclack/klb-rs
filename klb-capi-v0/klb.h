/*
* Copyright (C) 2014 by Fernando Amat
* See license.txt for full license and copyright notice.
*
* Authors: Lawrence Niu, Nathan Clack, Fernando Amat
*
*  Created on: October 2nd, 2014
*      Author: Fernando Amat
*
*    Modified: Jun 20 17
*          by: Vidrio Technologies <support at vidriotech dot com>
*              Lawrence Niu, Nathan Clack
*
* The interface defined here follows Amat's original C interface.
*/

#ifndef __KLB_IMAGE_C_WRAPPER_H__
#define __KLB_IMAGE_C_WRAPPER_H__

#ifdef __cplusplus
extern "C" {
#endif

#include <stdint.h>

  typedef float  float32_t;
  typedef double float64_t;

#define KLB_DATA_DIMS (5) //our images at the most have 5 dimensions: x,y,z,c,t
#define KLB_METADATA_SIZE (256) //number of bytes in metadata
#define KLB_DEFAULT_HEADER_VERSION (2)

  // Following mylib conventions here are the data types
  enum KLB_DATA_TYPE
  {
    UINT8_TYPE = 0,
    UINT16_TYPE = 1,
    UINT32_TYPE = 2,
    UINT64_TYPE = 3,
    INT8_TYPE = 4,
    INT16_TYPE = 5,
    INT32_TYPE = 6,
    INT64_TYPE = 7,
    FLOAT32_TYPE = 8,
    FLOAT64_TYPE = 9
  };

  //Compression type look up table (add to the list if you use a different one)
  //To add more compression types just add it here and look for 
  enum KLB_COMPRESSION_TYPE
  {
    NONE = 0,
    BZIP2 = 1,
    ZLIB = 2,
    ZSTD = 3
  };

#if defined(COMPILE_SHARED_LIBRARY) && defined(_MSC_VER)
#define DECLSPECIFIER __declspec(dllexport)
#define EXPIMP_TEMPLATE

#else

#define DECLSPECIFIER
#define EXPIMP_TEMPLATE

#endif



  DECLSPECIFIER int writeKLBstack(const void* im, const char* filename, uint32_t xyzct[KLB_DATA_DIMS], enum KLB_DATA_TYPE dataType, int numThreads, float32_t pixelSize[KLB_DATA_DIMS], uint32_t blockSize[KLB_DATA_DIMS], enum KLB_COMPRESSION_TYPE compressionType, char metadata[KLB_METADATA_SIZE]);

  /*
  \brief Same as writeKLBstack but we use a double pointer to have each slice in a separate address. xyzct[3] = xyzct[4] = 1 (no time or channel information). We assume xyzct[2] = number of slices
  */
  DECLSPECIFIER int writeKLBstackSlices(const void** im, const char* filename, uint32_t xyzct[KLB_DATA_DIMS], enum KLB_DATA_TYPE dataType, int numThreads, float32_t pixelSize[KLB_DATA_DIMS], uint32_t blockSize[KLB_DATA_DIMS], enum KLB_COMPRESSION_TYPE compressionType, char metadata[KLB_METADATA_SIZE]);

  DECLSPECIFIER int readKLBheader(const char* filename, uint32_t xyzct[KLB_DATA_DIMS], enum KLB_DATA_TYPE *dataType, float32_t pixelSize[KLB_DATA_DIMS], uint32_t blockSize[KLB_DATA_DIMS], enum KLB_COMPRESSION_TYPE *compressionType, char metadata[KLB_METADATA_SIZE]);

  /*
  \brief use dataType in order to know how to cast the returned pointer with the image content.

  Returns NULL if there was an issue

  All the parameters after numThreads (included) are optional
  */
  DECLSPECIFIER void* readKLBstack(const char* filename, uint32_t xyzct[KLB_DATA_DIMS], enum KLB_DATA_TYPE *dataType, int numThreads, float32_t pixelSize[KLB_DATA_DIMS], uint32_t blockSize[KLB_DATA_DIMS], enum KLB_COMPRESSION_TYPE *compressionType, char metadata[KLB_METADATA_SIZE]);


  /*
  \brief read image when you have already allocated memory and you want to reuse it (it could be good to read a time series where images are always the same size). You can use readKLBheader to extract all the metadata
  */
  DECLSPECIFIER int readKLBstackInPlace(const char* filename, void* im, enum KLB_DATA_TYPE *dataType, int numThreads);

  DECLSPECIFIER int readKLBroiInPlace(const char* filename, void* im, uint32_t xyzctLB[KLB_DATA_DIMS], uint32_t xyzctUB[KLB_DATA_DIMS], int numThreads);


#ifdef __cplusplus
}
#endif
#endif //end of __KLB_IMAGE_IO_H__
