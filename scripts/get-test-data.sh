#!/usr/bin/env bash
source_url="https://drive.google.com/uc?export=download&id=0B_ETWyvzjUXWRTQzZ3ltd09nblk"
destination="data/img.klb"
timeout_seconds=5;
mkdir -p data
curl -sSL -m $timeout_seconds -o $destination $source_url