#!/bin/bash
MYDIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )"
echo "$MYDIR"
MYSRC="$( cd "$MYDIR/../klb/src" && pwd )"
inotifywait -e modify "$MYSRC" |
while read -r directory events filename; do
    echo $filename
done