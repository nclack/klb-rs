/*
  Benchmarker:
    Links to klb.dll (or klb.lib on windows for whatever reason) and calls writeKLBstack and readKLBstack.
    Prints out image dimensions (hardcoded) and time elapsed for whichever call you chose.

  Usage:
    cargo run|<command> read|write
    Benches either read or write on "bench.klb"
*/
extern crate libc;
extern crate time;
extern crate rand;
extern crate crossbeam;

use libc::{c_void, c_char, uint32_t, c_int, c_float};

#[link(name = "klb")]
extern  {
  fn writeKLBstack(
    im: *mut c_void,
    filename: *const c_char,
    xyzct: *mut uint32_t, //sz KLB_DATA_DIMS
    dataType: c_int, //KLB_DATA_TYPE
    numThreads: c_int,
    pixelSize: *mut c_float, //sz KLB_DATA_DIMS
    blockSize: *mut uint32_t, //sz KLB_DATA_DIMS
    compressionType: c_int, //KLB_COMPRESSION_TYPE
    metadata: *mut c_char,
  ) -> c_int;

  fn readKLBstack(
    filename: *const c_char,
    xyzct: *mut uint32_t, //sz KLB_DATA_DIMS
    dataType: *mut c_int, //KLB_DATA_TYPE
    numThreads: c_int,
    pixelSize: *mut c_float, //sz KLB_DATA_DIMS
    blockSize: *mut uint32_t, //sz KLB_DATA_DIMS
    compressionType: *mut c_int, //KLB_COMPRESSION_TYPE
    metadata: *mut c_char,
  ) -> *mut c_void;
}

fn build_rand(vol: &[u8]) {
  extern crate num_cpus;
  use rand::Rng;
  let nt = if NTHREADS == -1 {
    num_cpus::get()
  } else {
    NTHREADS as usize
  };
  crossbeam::scope(|scope| for i in 0..nt {
    scope.spawn(move || {
      let mut rng = rand::thread_rng();
      let div = vol.len() / nt;
      let off = i * div;

      let len = if i == nt - 1 { vol.len() } else { off + div };

      let buf = vol as *const [u8] as *mut [u8];
      unsafe {
        rng.fill_bytes(&mut (*buf)[off..len]);
      }
    });
  });
}

const KLB_DATA_DIMS: usize = 5;
const KLB_METADATA_SIZE: usize = 256;
const NTHREADS: c_int = -1;

#[repr(C)]
#[allow(non_camel_case_types)]
#[allow(dead_code)]
enum KLB_DT {
  UINT8_TYPE = 0,
  UINT16_TYPE,
  UINT32_TYPE,
  UINT64_TYPE,
  INT8_TYPE,
  INT16_TYPE,
  INT32_TYPE,
  INT64_TYPE,
  FLOAT32_TYPE,
  FLOAT64_TYPE,
}

#[repr(C)]
#[allow(dead_code)]
enum KLB_CT {
  NONE = 0,
  BZIP2,
  ZLIB,
}

enum BenchMode {
  Read,
  Write,
}

fn main() {
  use std::{mem, slice, env};
  use std::ffi::CString;

  let mut args_iter = env::args().skip(1);

  let mode = match args_iter.next() {
    Some(s) => {
      match s.trim().to_lowercase().as_str() {
        "read" => BenchMode::Read,
        "write" => BenchMode::Write,
        _ => panic!("First arg must be read|write"),
      }
    }
    None => panic!("<executable> read|write"),
  };

  let filename = CString::new("bench.klb").unwrap();

  let mut xyzct: [uint32_t; KLB_DATA_DIMS] = [1024, 1024, 1024, 1, 1];
  let mut bs: [uint32_t; KLB_DATA_DIMS] = [64, 64, 8, 1, 1];
  let mut ps = [0.0 as c_float; KLB_DATA_DIMS];
  let dt = KLB_DT::UINT8_TYPE;
  let ct = KLB_CT::BZIP2;
  let mut md = [0 as c_char; KLB_METADATA_SIZE];

  match mode {
    BenchMode::Read => {
      println!("reading...");
      let im;
      let mut tm = time::precise_time_ns();
      unsafe {
        im = readKLBstack(
          filename.as_ptr(),
          &mut xyzct[0] as *mut uint32_t,
          &mut (dt as c_int) as *mut c_int,
          NTHREADS,
          &mut ps[0] as *mut c_float,
          &mut bs[0] as *mut uint32_t,
          &mut (ct as c_int) as *mut c_int,
          &mut md[0] as *mut c_char,
        );
      }
      tm = time::precise_time_ns() - tm;

      if im.is_null() {
        panic!("Error reading from file!");
      }

      println!("Reading {:?} Time {} ms", xyzct, tm as f64 / 1000000f64);
    }
    BenchMode::Write => {
      println!("allocating...");
      //use malloc for compatibility with C++ code.
      let mut nelem = mem::size_of::<u8>();
      for i in 0..KLB_DATA_DIMS {
        nelem *= xyzct[i] as usize;
      }
      let im = unsafe { libc::malloc(mem::size_of::<u8>() * nelem) };
      if im.is_null() {
        panic!("Critical Error: Insufficient Memory for Allocation");
      }
      build_rand(unsafe { slice::from_raw_parts(im as *mut u8, nelem) });

      let res;
      println!("writing");
      let mut tm = time::precise_time_ns();
      unsafe {
        res = writeKLBstack(
          im,
          filename.as_ptr(),
          &mut xyzct[0] as *mut uint32_t,
          dt as c_int,
          NTHREADS,
          &mut ps[0] as *mut c_float,
          &mut bs[0] as *mut uint32_t,
          ct as c_int,
          &mut md[0],
        );
      }
      tm = time::precise_time_ns() - tm;

      if res != 0 {
        panic!("Write Code {}", res);
      }

      println!("Writing {:?} Time {} ms", xyzct, tm as f64 / 1000000f64);
    }
  }
}
