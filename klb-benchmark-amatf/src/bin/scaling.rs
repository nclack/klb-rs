#![allow(non_snake_case)]
extern crate libc;
extern crate time;

use libc::{c_void, c_char, uint32_t, c_int, c_float};

#[link(name = "klb")]
extern  {
  fn writeKLBstack(
    im: *mut c_void,
    filename: *const c_char,
    xyzct: *mut uint32_t, //sz KLB_DATA_DIMS
    dataType: c_int, //KLB_DATA_TYPE
    numThreads: c_int,
    pixelSize: *mut c_float, //sz KLB_DATA_DIMS
    blockSize: *mut uint32_t, //sz KLB_DATA_DIMS
    compressionType: c_int, //KLB_COMPRESSION_TYPE
    metadata: *mut c_char,
  ) -> c_int;

  fn readKLBstack(
    filename: *const c_char,
    xyzct: *mut uint32_t, //sz KLB_DATA_DIMS
    dataType: *mut c_int, //KLB_DATA_TYPE
    numThreads: c_int,
    pixelSize: *mut c_float, //sz KLB_DATA_DIMS
    blockSize: *mut uint32_t, //sz KLB_DATA_DIMS
    compressionType: *mut c_int, //KLB_COMPRESSION_TYPE
    metadata: *mut c_char,
  ) -> *mut c_void;
}

const KLB_DATA_DIMS: usize = 5;
const KLB_METADATA_SIZE: usize = 256;

fn nbytes(shape:&[uint32_t],typeid:c_int) -> f64 {
    let bpp=match typeid {
        0 =>  1,
        1 =>  2,
        2 =>  4,
        3 =>  8,
        4 =>  1,
        5 =>  2,
        6 =>  4,
        7 =>  8,
        8 =>  4,
        9 =>  8,
        _ => unimplemented!()
    };
    shape.iter().fold(1f64,|acc,&s|acc*(s as f64))*(bpp as f64)
}


#[derive(Debug)]
#[repr(packed)]
struct Context {
    xyzct: [uint32_t; KLB_DATA_DIMS], //sz KLB_DATA_DIMS
    dataType: c_int, //KLB_DATA_TYPE
    pixelSize: [c_float; KLB_DATA_DIMS], //sz KLB_DATA_DIMS
    blockSize: [uint32_t; KLB_DATA_DIMS], //sz KLB_DATA_DIMS
    compressionType: c_int, //KLB_COMPRESSION_TYPE
    im: *mut c_void
}

fn read(filename:&str,nthreads:usize) -> Context {
    let mut ctx:Context=unsafe{std::mem::uninitialized()};                
    let mut metadata: [c_char;KLB_METADATA_SIZE]=[0;KLB_METADATA_SIZE];
    let start = time::precise_time_ns();                   
    ctx.im=unsafe {
        let filename=std::ffi::CString::new(filename).unwrap();
        let p = readKLBstack(filename.as_ptr() as *const i8,
                            &mut ctx.xyzct[0] as *mut uint32_t,
                            &mut ctx.dataType as *mut c_int,
                            nthreads as i32,
                            &mut ctx.pixelSize[0] as *mut c_float,
                            &mut ctx.blockSize[0] as *mut uint32_t,
                            &mut ctx.compressionType as *mut c_int,
                            &mut metadata[0] as *mut c_char);                      
        if p.is_null() {
            panic!("Error reading from file!");
        }
        p
    };
    // println!("READ {:?}",ctx);
    let end = time::precise_time_ns();
    let nbytes=nbytes(&ctx.xyzct,ctx.dataType);
    println!("{} threads: Reading {:?} Time {} ms {} MB/s",nthreads, ctx.xyzct, (end-start) as f64 / 1000000f64,1000f64*nbytes/(end-start) as f64);
    ctx
}


fn write(ctx:&mut Context,nthreads:usize) {
        // println!("WRITE {:?}",ctx);
        let mut metadata: [c_char;KLB_METADATA_SIZE]=[0;KLB_METADATA_SIZE];
                
        let filename=std::ffi::CString::new("out.klb").unwrap();
        let start = time::precise_time_ns();
        unsafe {            
            writeKLBstack(
                ctx.im,
                filename.as_ptr() as *const i8,
                &mut ctx.xyzct[0] as *mut uint32_t,
                ctx.dataType,
                nthreads as i32,
                &mut ctx.pixelSize[0] as *mut c_float,
                &mut ctx.blockSize[0] as *mut uint32_t,
                ctx.compressionType,
                &mut metadata[0] as *mut c_char,
                );
        }
        let end = time::precise_time_ns();
        let nbytes=nbytes(&ctx.xyzct,ctx.dataType);
        println!("{} threads: Writing {:?} Time {} ms {} MB/s",nthreads, ctx.xyzct, (end-start) as f64 / 1000000f64,1000f64*nbytes/(end-start) as f64);
}

fn main() {
    println!("HERE");
    if let Some(filename) = std::env::args().nth(1) {
        let mut ctx=read(&filename,56);
        for n in (1..56/2).map(|x|{x*2}) {
            write(&mut ctx,n as usize);
        }
    } else {
        println!("expected a filename as an argument.");
    }
}