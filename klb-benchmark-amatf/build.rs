use std::path::Path;

fn main() {
    let root=Path::new(file!()).parent().unwrap().to_str().unwrap();
    println!(r"cargo:rustc-link-search={}",root);
}